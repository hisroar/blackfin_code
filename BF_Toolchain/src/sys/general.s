//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                              general.s                                   //
//                           General Functions                               //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains general functions to be used by any file. The functions
    included are:
        _divide             - divides R0 by R1, returns the quotient in R2 and
                              remainder in R3. (PUBLIC)
        _dec2string         - converts a passed decimal number to a string. (PUBLIC)
    Revision History:
    05/07/16    Dennis Shim     initial revision
*/

.global _divide
.global _dec2string

// Include constants from general.inc
.include "src/sys/general.inc"

/*
_divide

Description:        Divides R0 by R1, and returns the quotient in R2 and remainder
                    in R3. Assumes R0 and R1 are unsigned integers.

Operation:          Perform the division algorithm. Initially set R2 = 0. Check
                    if R0 < R1. If it is, stop looping and copy R1 into R3.
                    Otherwise subtract R1 from R0 and increment R2. 

Arguments:          dividend  - R0 - number to divide
                    divisor   - R1 - number to divide by
Return Value:       quotient  - R2 - result of the division
                    remainder - R3 - remainder of the division

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16    Dennis Shim      initial revision
*/
_divide:
    [--SP] = R0;                        // preserve registers
    
    R2 = 0;                             // initialize quotient to be zero

DivideLoopCondition:                    // check if dividend is less than divisor
    CC = R0 < R1;
    if CC   JUMP DivideFinished;        // dividend is less, stop dividing
    //if !CC  JUMP DivideLoopBody;      // dividend is still greater, keep dividing
    
DivideLoopBody:
    R2 += 1;                            // increment R3 because we have another
                                        //   R1 in R0
    R0 = R0 - R1;                       // subtract R1 from R0 to get the next
                                        //   to keep dividing
    JUMP DivideLoopCondition;           // keep looping
    
DivideFinished:
    R3 = R0;                            // remainder is what is left in R0
    
    R0 = [SP++];                        // restore registers

	RTS;

._divide.END:

/*
_dec2string

Description:        Converts a number in decimal to a string. Assumes the number
                    is positive. The string begins at the specified address.
                    Returns the address of the null character.

Operation:          Check if the number is non-zero. Loop until it is zero.
                    Divide the number by the largest possible factor of 10 that
                    can fit in 32 bits (2^32 ~ 4 billion, so 10^9 is what we want).
                    If this number is non-zero, convert to ASCII and write it to
                    the string address. Make the number its modulo. Keep doing
                    this for lower factors of 10 until the number is zero.

Arguments:          number  - R0 - number to convert
                    str_adr - R1 - address to put the string in
Return Value:       last_adr - R0 - address of the null character

Local Variables:    number - R0 - current number being divided.
                    address - P0 - address of the character to write to.
                    fof10 - R1 - largest factor of 10
                    first_digit - R4 - 1 if first digit has been encountered, 0
                                       if all previous digits were zeroes
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16    Dennis Shim      initial revision
*/
_dec2string:
    [--SP] = P0;                        // preserve registers
    [--SP] = R4;
    [--SP] = R3;
    [--SP] = R2;
    [--SP] = R0;

    CC = R0 == 0;                       // check if the number is zero
    if CC   JUMP NumZero;               // number is zero, just write a zero
    
    P0 = R1;                            // prepare to write the string to the
                                        //   correct address
    R1.L = LARGEST_F_OF_10;             // prepare to divide number with factor
    R1.H = LARGEST_F_OF_10;             //   of 10.
    R4 = 0;                             // we have still only seen zeroes

ConversionLoopCondition:                // check if number is zero yet
    CC = R0 == 0;
    if CC   JUMP ConversionFinished;    // number is zero, stop converting
    //if !CC  JUMP ConversionLoopBody;  // number is non-zero, keep converting
    
ConversionLoopBody:                     // divide the number by the largest
                                        //   factor of 10
                                        // call _divide to divide num by fof10
    [--SP] = RETS;                      // preserve return address
    CALL _divide;
    RETS = [SP++];                      // restore return address

    R0 = R3;                            // set new num to be number modulo fof10

    CC = R2 == 0;                       // check if quotient is zero
    //if CC   JUMP CheckFirstDigit;     // quotient is zero, check if we've had
                                        //   digits written before
    if !CC  JUMP WriteChar;             // quotient is non-zero, write the digit

CheckFirstDigit:                        // check if there were any non-zero
                                        //   digits prior to this zero
    CC = R4 == 0;                       // check if all previous digits were zeroes
    if CC   JUMP ConversionLoopNext;    // they were, don't write, just keep looping
    //if !CC  JUMP WriteChar;           // they weren't, write the zero

WriteChar:
    R4 = 1;                             // we've definitely seen a non-zero digit

    R2 += '0'                           // prepare to write the ASCII char
    B[P0] = R2;                         // write the ASCII char
    P0 += 1;                            // prepare to write to the next char
    
ConversionLoopNext:                     // prepare to get the next fof10
                                        // now we need to divide fof10 by 10
    [--SP] = R0;                        // preserve num

    R0 = R1;                            // fof10 is the dividend
    R1 = 10;                            // 10 is the divisor
    [--SP] = RETS;                      // preserve return address
    CALL _divide;
    RETS = [SP++];                      // restore return address
    R1 = R2;                            // set fof10 to the quotient of fof10/10

    R0 = [SP++];                        // restore num

    JUMP ConversionLoopCondition;       // keep looping

NumZero:                                // number is zero, just write zero
    R0 = '0';                           // prepare to write
    B[P0] = R0;                         // write zero to the string
    P0 += 1;                            // make sure to go to next char to write null

ConversionFinished:
    R0.L = ASCII_NULL;                  // prepare to write null char
    R0.H = ASCII_NULL;
    B[P0] = R0;                         // terminate the string with null char

    R1 = P0;
    
    R0 = [SP++];                        // restore registers
    R2 = [SP++];
    R3 = [SP++];
    R4 = [SP++];
    P0 = [SP++];

    RTS;

._dec2string.END:

// Data segment
.data

.end
