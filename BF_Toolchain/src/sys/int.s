//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                int.s                                     //
//                         Interrupt Functions                              //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for initializing interrupts. The functions
    included are:
        _interrupt_init         - Initializes interrupts. (PUBLIC)
        _disable_SPI            - Disables the SPI interrupt. (PUBLIC)
        _enable_SPI             - Enables the SPI interrupt. (PUBLIC)
        _disable_PFB            - Disables the PF B interrupt. (PUBLIC)
        _enable_PFB             - Enables the PF B interrupt. (PUBLIC)

    Revision History:
    04/28/16    Dennis Shim     initial revision
    06/01/16    Dennis Shim     added functions to disable/enable specific interrupts
    06/14/16    Dennis Shim     updated comments
*/

.global _interrupt_init
.global _core_timer_handler
.global _disable_SPI
.global _enable_SPI
.global _disable_PFB
.global _enable_PFB

/* Include constants from int.inc */
.include "src/sys/int.inc"

/*
_interrupt_init

Description:        Initialize the Blackfin interrupts. The interrupts that are
                    enabled are the PF Interrupt A and the Core Timer interrupts.

Operation:          Write the correct values to the SIC_IMASK and IMASK registers
                    to enable the PF Interrupt A and Core Timer interrupts.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             SIC_IMASK and IMASK registers are changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_interrupt_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Initialize SIC_IMASK register - enable PF Interrupt A
    P0.L = SIC_IMASK_ADR;               // get SIC_IMASK register address
    P0.H = SIC_IMASK_ADR;
    R0.L = SIC_IMASK_VAL;               // get SIC_IMASK value
    R0.H = SIC_IMASK_VAL;
    
    [P0] = R0;                          // write SIC_IMASK value to the address
    
    // Initialize IMASK register - enable core timer interrupts
    P0.L = IMASK_ADR;                   // get IMASK register address
    P0.H = IMASK_ADR; 
    R0.L = IMASK_VAL;                   // get IMASK value
    R0.H = IMASK_VAL;
    
    [P0] = R0;                          // write IMASK value to the address

    // Initialize SIC_IAR1 register
    P0.L = SIC_IAR1_ADR;                // get SIC_IAR1 register address
    P0.H = SIC_IAR1_ADR; 
    R0.L = SIC_IAR1_VAL;                // get SIC_IAR1 value
    R0.H = SIC_IAR1_VAL;
    
    [P0] = R0;                          // write SIC_IAR1 value to the address

    // Initialize SIC_IAR2 register
    P0.L = SIC_IAR2_ADR;                // get SIC_IAR2 register address
    P0.H = SIC_IAR2_ADR; 
    R0.L = SIC_IAR2_VAL;                // get SIC_IAR2 value
    R0.H = SIC_IAR2_VAL;
    
    [P0] = R0;                          // write SIC_IAR2 value to the address
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._interrupt_init.END:

/*
_disable_SPI

Description:        Disable the SPI interrupt.

Operation:          Masks out the SPI enable bit in the SIC_MASK register.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             SIC_IMASK register is changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/01/16    Dennis Shim     initial revision
*/
_disable_SPI:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    [--SP] = R0;

    // Initialize SIC_IMASK register - enable PF Interrupt A
    P0.L = SIC_IMASK_ADR;               // get SIC_IMASK register address
    P0.H = SIC_IMASK_ADR;
    R0 = [P0];                          // get SIC_IMASK value
    
    R1.L = DISABLE_SPI;                 // prepare to disable SPI interrupt
    R1.H = DISABLE_SPI;

    R0 = R0 & R1;                       // mask out the SPI interrupt

    [P0] = R0;                          // write the value to the IMASK register
    
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    P0 = [SP++];

    RTS;

._disable_SPI.END:

/*
_enable_SPI

Description:        Enable the SPI interrupt.

Operation:          Enable the SPI enable bit in the SIC_MASK register.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             SIC_IMASK register is changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/01/16    Dennis Shim     initial revision
*/
_enable_SPI:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    [--SP] = R0;

    // Initialize SIC_IMASK register - enable PF Interrupt A
    P0.L = SIC_IMASK_ADR;               // get SIC_IMASK register address
    P0.H = SIC_IMASK_ADR;
    R0 = [P0];                          // get SIC_IMASK value
    
    R1.L = ENABLE_SPI;                  // prepare to disable SPI interrupt
    R1.H = ENABLE_SPI;

    R0 = R0 | R1;                       // mask out the SPI interrupt

    [P0] = R0;                          // write the value to the IMASK register
    
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    P0 = [SP++];

    RTS;

._enable_SPI.END:

/*
_disable_PFB

Description:        Disable the PFB interrupt.

Operation:          Masks out the PFB enable bit in the SIC_MASK register.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             SIC_IMASK register is changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/01/16    Dennis Shim     initial revision
*/
_disable_PFB:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    [--SP] = R0;

    // Initialize SIC_IMASK register - enable PF Interrupt A
    P0.L = SIC_IMASK_ADR;               // get SIC_IMASK register address
    P0.H = SIC_IMASK_ADR;
    R0 = [P0];                          // get SIC_IMASK value
    
    R1.L = DISABLE_PFB;                 // prepare to disable PFB interrupt
    R1.H = DISABLE_PFB;

    R0 = R0 & R1;                       // mask out the PFB interrupt

    [P0] = R0;                          // write the value to the IMASK register
    
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    P0 = [SP++];

    RTS;

._disable_PFB.END:

/*
_enable_PFB

Description:        Enable the PFB interrupt.

Operation:          Enable the PFB enable bit in the SIC_MASK register.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             SIC_IMASK register is changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/01/16    Dennis Shim     initial revision
*/
_enable_PFB:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    [--SP] = R0;

    // Initialize SIC_IMASK register - enable PF Interrupt A
    P0.L = SIC_IMASK_ADR;               // get SIC_IMASK register address
    P0.H = SIC_IMASK_ADR;
    R0 = [P0];                          // get SIC_IMASK value
    
    R1.L = ENABLE_PFB;                  // prepare to disable PFB interrupt
    R1.H = ENABLE_PFB;

    R0 = R0 | R1;                       // mask out the PFB interrupt

    [P0] = R0;                          // write the value to the IMASK register
    
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    P0 = [SP++];

    RTS;

._enable_PFB.END:

.data

.end
