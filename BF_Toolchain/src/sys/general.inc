##############################################################################
#                                                                            #
#                                general.inc                                 #
#                             General Constants                              #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains general constants.
#
# Revision History:
# 	05/07/16    Dennis Shim     initial revision
#

.equ LARGEST_F_OF_10, 1000#1000000000 	# largest factor of 10 that can divide in
										#   2^32 ~ 4 billion
.equ ASCII_NULL, 0						# value for ASCII null character

.equ FALSE, 0							# value for boolean TURE
.equ TRUE, 1 							# value for boolean FALSE