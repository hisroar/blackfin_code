//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                 crt0.s                                   //
//                                                                          //
// Initialization file for EE52 Blackfin MP3 project. It sets up the IRQ    //
// vector table and sets the processor to supervisor mode. The user must    //
// implement project specific initialization such as clock setup and        //
// calling initialization functions.                                        //
//                                                                          //
// Revision History:                                                        //
//                                                                          //
// 2013/03/24   Josh Fromm      Initial revision                            //
// 2016/04/21   Dennis Shim     Added initialization                        //
// 2016/04/28   Dennis Shim     Added interrupt, keypad, timer              //
// 2016/05/07   Dennis Shim     Added display                               //
// 2016/05/19   Dennis Shim     Added IDE                                   //
// 2016/05/26   Dennis Shim     Added audio                                 //   
//////////////////////////////////////////////////////////////////////////////

/* Include constants from crt0.inc */
.include "src/sys/crt0.inc"

/* The .ALIGN directive forces the address alignment of an instruction or
   data item. Here, ensure the first element of the section is aligned.*/
.ALIGN 4;

/* Set the scope of the '_main' symbol to global. This makes the symbol
   available for reference in object files that are linked to the current
   one. Use '.EXTERN' to refer to a global symbol from another file.*/

/* EXTERNS */
.extern _main  /* need to jump to the mainloop after initializing */
.extern _interrupt_init 		// initialize interrupts
.extern _install_core_timer_EH 	// install core timer handler
.extern _core_timer_init		// initialize core timer
.extern _install_PF_A_EH		// install PF A interrupt handler
.extern _PF_init				// initialize PFs
.extern _keypad_init            // initialize keypad
.extern _display_init           // initialize display
.extern _ide_init               // initialize ide 

.global __start /* allow other files to see this function */

/////////////////////////////////////////////////////////////////
// standard
.equ IVB, 0xFFE02000
.equ UNASSIGNED_VAL, 0x8181
.equ INTERRUPT_BITS, 0x400	// just IVG15
.equ SYSCFG_VALUE, 0x30
.equ STACK_MEM, 0xFFB01000
.equ IPEND, 0xFFE02108
.equ IMASK, 0xFFE02104

	.text;
__start:

    SP.L = STACK_MEM;
    SP.H = STACK_MEM;
    usp = sp;

	// Initialise the Event Vector table.
	P0.H = 0xFFE0;
	P0.L = 0x2000;

    // Install __unknown_exception_occurred in EVT so that
	// there is defined behaviour.
	P0 += 2*4;		// Skip Emulation and Reset
	P1 = 13;
	R1.L = __unknown_exception_occurred;
	R1.H = __unknown_exception_occurred;
	LSETUP (.ivt,.ivt) LC0 = P1;
    .ivt:	[P0++] = R1;

	// Set IVG15's handler to be the start of the mode-change
	// code. Then, before we return from the Reset back to user
	// mode, we'll raise IVG15. This will mean we stay in supervisor
	// mode, and continue from the mode-change point, but at a
	// much lower priority.
	P1.H = supervisor_mode;
	P1.L = supervisor_mode;
	[P0] = P1;

    // We're still in supervisor mode at the moment, so the FP
	// needs to point to the supervisor stack.
	FP = SP;

	// Make space for incoming "parameters" for functions
	// we call from here:
	SP += -12;

	// When the processor is turned on, it is in reset mode which is one of
	// the highest priority interrupts. While in reset mode, the processor will
	// not service any other interrupt types. Instead of reset mode, we'd like
	// the processor to be in the lowest priority mode possible. To get to that
	// mode, we first raise the IVG15 interrupt flag. However, the processors
	// current priority will not allow IVG15 to be triggered.

	// Enable IVG
	R4 = INTERRUPT_BITS;
	R4 <<= 5;
	STI R4;
	RAISE 15;

	// In order to leave reset mode, we perform an RTI which jumps to the
	// usermode label. Usermode is a simple infinite loop, however once the RTI
	// is processed, the processor will acknowledge IVG15. Since we earlier
	// set the handler of IVG15 to be supervisor_mode, the processor will
	// immediately handle IVG15 and end up at the supervisor_mode label. From
	// this point on, the processor will be in the lowest interrupt priority
	// level which is pretty neat.

	P0.L = usermode;
	P0.H = usermode;
	RETI = P0;

usermode:
	NOP;
	RTI;

supervisor_mode:

	// enable all other interrupts, pushing RETI globally enables interrupts
	// for some reason
	[--SP] = RETI;
	FP = SP;
	SP += 12;

my_init:
    
    // Initialize PLL_CTL register
    P0.L = PLL_CTL_ADR;
    P0.H = PLL_CTL_ADR;
    R0.L = PLL_CTL_VAL;
    
    W[P0] = R0.L;
    
    // Initialize PLL_DIV register
    P0.L = PLL_DIV_ADR;
    P0.H = PLL_DIV_ADR;
    R0.L = PLL_DIV_VAL;
    
    W[P0] = R0.L;
    
    // Initialize EBIU_AMGCTL register
    P0.L = EBIU_AMGCTL_ADR;
    P0.H = EBIU_AMGCTL_ADR;
    R0.L = EBIU_AMGCTL_VAL;
    
    W[P0] = R0.L;
    
    // Initialize EBIU_AMBCTL0 register
    P0.L = EBIU_AMBCTL0_ADR;
    P0.H = EBIU_AMBCTL0_ADR;
    R0.L = EBIU_AMBCTL0_VAL;
    R0.H = EBIU_AMBCTL0_VAL;
    
    [P0] = R0;
    
    // Initialize EBIU_AMBCTL1 register
    P0.L = EBIU_AMBCTL1_ADR;
    P0.H = EBIU_AMBCTL1_ADR;
    R0.L = EBIU_AMBCTL1_VAL;
    R0.H = EBIU_AMBCTL1_VAL;
    
    [P0] = R0;
    
    // INITIALIZATIONS
    [--SP] = RETS;					// preserve return address
    CALL _install_core_timer_EH; 	// install core timer event handler
    RETS = [SP++];					// restore return address

    [--SP] = RETS;					// preserve return address
    CALL _core_timer_init; 			// initialize core timer
    RETS = [SP++];					// restore return address

    [--SP] = RETS;					// preserve return address
    CALL _install_PF_A_EH; 			// install PF A interrupt event handler
    RETS = [SP++];					// restore return address

    [--SP] = RETS;                  // preserve return address
    CALL _install_PF_B_EH;          // install PF B interrupt event handler
    RETS = [SP++];                  // restore return address

    [--SP] = RETS;                  // preserve return address
    CALL _install_SPI_EH;           // install SPI event handler
    RETS = [SP++];                  // restore return address

    [--SP] = RETS;					// preserve return address
    CALL _PF_init; 					// initialize PFs
    RETS = [SP++];					// restore return address
    
    [--SP] = RETS;					// preserve return address
    CALL _interrupt_init; 			// initialize interrupts
    RETS = [SP++];					// restore return address
    
    [--SP] = RETS;					// preserve return address
    CALL _keypad_init; 			    // initialize keypad
    RETS = [SP++];					// restore return address
    
    [--SP] = RETS;					// preserve return address
    CALL _display_init; 			// initialize display
    RETS = [SP++];					// restore return address
    
    [--SP] = RETS;					// preserve return address
    CALL _ide_init; 			    // initialize ide
    RETS = [SP++];					// restore return address

    [--SP] = RETS;                  // preserve return address
    CALL _SPI_init;                 // initialize SPI
    RETS = [SP++];                  // restore return address
    
    [--SP] = RETS;                  // preserve return address
    CALL _dram_clear;               // clear DRAM
    RETS = [SP++];                  // restore return address

	CALL.X _main; /* Jump to Glen Code */
	RTS;

/* Delimit the '_main' symbol so the linker knows which code is associated
   with the symbol. */
.__start.END:

// Default exception handling does pretty much nothing besides sit in a loop
__unknown_exception_occurred:
    R0 = R0;
	RTX;
.__unknown_exception_occurred.end:

.data

.end
