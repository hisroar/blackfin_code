//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                tmr.s                                     //
//                           Timer Functions                                //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for initializing timer interrupts. The functions
    included are:
        _core_timer_init            - Initializes the core timer interrupts. (PUBLIC)
        _core_timer_event_handler   - Event handler for the core timer interrupts.
                                      It debounces keypresses. (PUBLIC)
        _install_core_timer_EH      - Installs event handler for core timer
                                      interrupts. (PUBLIC)
    Revision History:
    04/28/16    Dennis Shim     initial revision
    05/26/16    Dennis Shim     added DRAM refresh
    06/04/16    Dennis Shim     added elapsed_time
*/

.global _core_timer_init
.global _core_timer_handler
.global _install_core_timer_EH

/* Include constants from tmr.inc */
.include "src/sys/tmr.inc"

/*
_core_timer_init

Description:        Initialize the Blackfin Core Timer. The timer will interrupt
                    at 1 kHz with the appropriate values written to the TCNTL,
                    TPERIOD, and TSCALE registers.

Operation:          Write the correct value to the TCNTL control register to
                    turn the core timer on. Write the correct value of TPERIOD
                    to the TPERIOD register to get interrupts at 1 kHz. Write
                    the correct scale value to the TSCALE register.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             TCNTL, TPERIOD, and TSCALE registers are changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_core_timer_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Initialize TCNTL register - turn timer on
    P0.L = TCNTL_ADR;                   // get TCNTL register address
    P0.H = TCNTL_ADR;
    R0.L = TCNTL_VAL;                   // get TCNTL value
    R0.H = TCNTL_VAL;
    
    [P0] = R0;                          // write TCNTL value to the address
    
    // Initialize TPERIOD register - set timer to 1 kHz
    P0.L = TPERIOD_ADR;                 // get TPERIOD register address
    P0.H = TPERIOD_ADR; 
    R0.L = TPERIOD_VAL;                 // get TPERIOD value
    R0.H = TPERIOD_VAL;
    
    [P0] = R0;                          // write TPERIOD value to the address
    
    // Initialize TSCALE register - set scale value to 1
    P0.L = TSCALE_ADR;                  // get TSCALE register address
    P0.H = TSCALE_ADR;
    R0.L = TSCALE_VAL;                  // get TSCALE value
    R0.H = TSCALE_VAL;
    
    [P0] = R0;                          // write TSCALE value to the address
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._core_timer_init.END:

/*
_core_timer_event_handler

Description:        Core timer event handler. Calls _keypress_scanner, which
                    checks if a key is being pressed and debounces it. 

Operation:          Preserve all registers. Debounce the select key and the shift
                    key. Refresh DRAM. Increment the elapsed time. Restore all
                    registers.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None. 
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    04/28/16    Dennis Shim     initial revision
    05/26/16    Dennis Shim     added DRAM refresh
    06/04/16    Dennis Shim     added elapsed_time
*/
_core_timer_event_handler:
    [--SP] = ASTAT;                     // preserve registers
    [--SP] = RETI;
    
    // debounce the select key
    [--SP] = RETS;					    // preserve return address
    CALL _key_debounce_select;
    RETS = [SP++];                      // restore return address
    
    // debounce the shift key
    [--SP] = RETS;					    // preserve return address
    CALL _key_debounce_shift;
    RETS = [SP++];                      // restore return address
    
    // debounce the encoder time delay
    [--SP] = RETS;					    // preserve return address
    CALL _encoder_time_debounce;
    RETS = [SP++];                      // restore return address
    
    // refresh DRAM
    [--SP] = RETS;                      // preserve return address
    CALL _dram_refresh;
    RETS = [SP++];                      // restore return address
    
    // increment ElapsedTime
    [--SP] = RETS;                      // preserve return address
    CALL _increment_elapsed_time;
    RETS = [SP++];                      // restore return address
    
    RETI = [SP++];                      // restore registers
    ASTAT = [SP++]; 
    
    RTI;
    
._core_timer_event_handler.END:

/*
_install_core_timer_EH

Description:        Installs the core timer interrupt event handler in the event
                    vector table.

Operation:          Installs the core timer event handler in the event vector table.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_install_core_timer_EH:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    P0.L = EVT6_ADR;                    // get the address of EVT6
    P0.H = EVT6_ADR;
    R0.L = _core_timer_event_handler;   // put the core timer event handler in EVT 6
    R0.H = _core_timer_event_handler;
 
    [P0] = R0;
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._install_core_timer_EH.END:

.data

.end
