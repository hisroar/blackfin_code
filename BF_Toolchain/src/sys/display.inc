##############################################################################
#                                                                            #
#                                display.inc                                 #
#                             Display Constants                              #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for the display.
#
# Revision History:
# 	05/07/16    Dennis Shim     initial revision
#

# Delay Timings:
.equ DELAY_POWER, 10000000			# wait >40 ms after power applied
.equ DELAY_WAKE_UP_1, 1000000 		# wait 5 ms after first wake up
.equ DELAY_WAKE_UP_2, 1000000 		# wait 160 µs after second/third wake up
.equ DELAY_COMMAND, 1000 			# enable pulse width needs to be > 300 ns
.equ DELAY_BUSY, 50000             	# wait a bit between busy flag set

# PF values bits: 0   0   0   0   0   E R/W  RS DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0
.equ DISPLAY_WAKE_UP, 0x0030 		# command to wake up display
.equ DISPLAY_CLEAR, 0x0001 			# clear the display
.equ DISPLAY_RET_HOME, 0x0002 		# return home
.equ DISPLAY_ENTRY_MODE, 0x0007 	# set cursor moving direction and display blinking
.equ DISPLAY_CONTROL, 0x000c 		# display on, cursor on, blinking on
.equ DISPLAY_CURSOR_SET, 0x0010 	# cursor moving
.equ DISPLAY_FUNCTION_SET, 0x0038 	# function set 8-bit & 2-line
.equ DISPLAY_CGRAM_ADR_SET, 0x0040 	# set the CGRAM address (OR address to lower 6 bits)
.equ DISPLAY_DDRAM_ADR_SET, 0x0080 	# set the DDRAM address (OR address to lower 7 bits)
.equ DISPLAY_BUSY_FLAG_READ, 0x0200 # read the busy flag (R/W = 1)
.equ DISPLAY_WRITE_DATA, 0x0100 	# write data to the address (OR data to lower 8 bits)
.equ DISPLAY_READ_DATA, 0x0300 		# read data from RAM

.equ DISPLAY_E_ENABLE, 0x0400 		# OR with this value to enable E bit
.equ DISPLAY_E_DISABLE, 0xFBFF	 	# AND with this value to disable E bit

.equ DISPLAY_LINE_1_ADR, 0x00 		# address of the first char in first line
.equ DISPLAY_LINE_2_ADR, 0x40 		# address of the first char in second line
.equ DISPLAY_LINE_3_ADR, 0x14 		# address of the first char in third line
.equ DISPLAY_LINE_4_ADR, 0x54 		# address of the first char in fourth line
.equ DISPLAY_LINE_LENGTH, 0x14 		# number of chars in each line of display
.equ DISPLAY_NUM_LINES, 4 			# number of lines in the display

# Constants
.equ SECONDS_IN_HOUR, 3600 			# number of seconds in an hour
.equ MINUTES_IN_HOUR, 60 			# number of minutes in an hour
.equ SECONDS_IN_MINUTE, 60 			# number of seconds in a minute

.equ LOWER_BIT_MASK, 0x00000001     # keep only the lowest bit after AND

.equ STRING_BUFFER_LENGTH, 21 		# number of bytes in string buffer (# chars
									#   in display line + 1)

.equ PREFACE_STRING_LENGTH, 8       # number of characters in each of the preface
                                    #   strings
.equ STATUS_STRING_LENGTH, 12       # number of characters in each of the status/key
                                    #   strings

                                    
# Status codes
.equ STATUS_PLAY, 0                 # output "Play" to display
.equ STATUS_FASTFWD, 1              # output "Fast Forward" to display
.equ STATUS_REVERSE, 2              # output "Reverse" to display
.equ STATUS_IDLE, 3                 # output "Idle" to display
.equ STATUS_ILLEGAL, 4              # never should be gotten
                                    
# String addresses
.equ TIME_ADR, DISPLAY_LINE_1_ADR 	# write time on the first line
.equ STATUS_ADR, DISPLAY_LINE_2_ADR # write status on the second line
.equ TITLE_ADR, DISPLAY_LINE_3_ADR 	# write title on the third line
.equ ARTIST_ADR, DISPLAY_LINE_4_ADR # write artist on the fourth line
.equ ENCODER_ADR, DISPLAY_LINE_4_ADR# write encoder on the fourth line

# Formula to get these dumb numbers:
# (Floor(Line #) / 2) * 0x14 + (1 - ((Line #) MODULO 2)) * 0x40