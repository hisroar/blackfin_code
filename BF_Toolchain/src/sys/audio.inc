##############################################################################
#                                                                            #
#                                audio.inc                                   #
#                             Audio Constants                                #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for playing audio.
#
# Revision History:
# 	05/26/16    Dennis Shim     initial revision
#

# SPI Baud Rate Register (SPI_BAUD)
.equ SPI_BAUD_ADR, 0xFFC00514	# address of the SPI_BAUD register
.equ SPI_BAUD_VAL, 100 			# 20 MHz / (10 * 2) = 1 MHz


# SPI Control Register (SPI_CTL)
.equ SPI_CTL_ADR, 0xFFC00500 	# address of the SPI_CTL register
.equ SPI_CTL_VAL, 0x5805		# value of the SPI_CTL register
								# 0--- ---- 00-- ----  reserved
								# -1-- ---- ---- ----  enable SPI
								# --0- ---- ---- ----  normal (not open drain)
								# ---1 ---- ---- ----  master
								# ---- 0--- ---- ----  active high SCK
								# ---- -0-- ---- ----  slave select pins controlled by hardware
								# ---- --0- ---- ----  MSB first
								# ---- ---0 ---- ----  8 bits
								# ---- ---- --0- ----  MOSI
								# ---- ---- ---0 ----  disable slave select
								# ---- ---- ---- 0---  doesn't matter
								# ---- ---- ---- -1--  send zeroes b/c otherwise just last note
								# ---- ---- ---- --01  start transfer w/ write of SPI_TDBR
.equ SPI_DISABLE, 0x7FFF 		# AND this w/ SPI_CTL value to disable SPI
.equ SPI_ENABLE, 0x8000 		# OR with this value to enable SPI

# SPI Status Register (SPI_STAT)
.equ SPI_STAT_ADR, 0xFFC00508 	# address of the SPI_STAT register
.equ SPI_STAT_VAL, 0x0001 		# value of the SPI_STAT register
								# initialize to this to reset the register

# SPI Transmit Data Buffer Register (SPI_TDBR)
.equ SPI_TDBR_ADR, 0xFFC0050C 	# address of the SPI_TDBR register

# SPI Receive Data Buffer Register (SPI_RDBR)
.equ SPI_RDBR_ADR, 0xFFC00510 	# address of the SPI_RDBR register

# Event Vector Table - IVG10
.equ EVT10_ADR, 0xFFE02028 			# address of EVT12 (for PF interrupt A)
