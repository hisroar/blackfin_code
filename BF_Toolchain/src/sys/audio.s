//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                audio.s                                   //
//                            Audio Functions                               //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for writing audio. The functions included are:
        _update                 - Updates the buffers if necessary. (PUBLIC)
        _audio_play             - Begins playing the audio from the passed buffer.
                                  (PUBLIC)
        _audio_halt             - Stops playing audio. (PUBLIC)
        _SPI_init               - Initializes SPI registers and enables SPI. (PUBLIC)
        _SPI_handler            - Event handler for SPI. Checks demand and sends a
                                  byte if demand is set.
        _demand_handler         - Event handler for demand (PF B). Sends a byte and
                                  enables SPI. (PUBLIC)
        _send_byte              - Sends a byte over SPI.
        _elapsed_time           - Returns the elapsed time in ms since the last time
                                  elapsed_time was called. (PUBLIC)
        _increment_elapsed_time - Increments ElapsedTime. (PUBLIC)

    Revision History:
    05/26/16    Dennis Shim     initial revision
    06/2-4/16   Dennis Shim     added more functions
    06/14/16    Dennis Shim     updated comments, and added TRUE/FALSE
*/

/* Include constants from audio.inc, int.inc, pf.inc, general.inc */
.include "src/sys/audio.inc"
.include "src/sys/int.inc"
.include "src/sys/pf.inc"
.include "src/sys/general.inc"

.global _update
.global _audio_play
.global _audio_halt
.global _SPI_init
.global _demand_handler
.global _elapsed_time
.global _increment_elapsed_time
.global _install_SPI_EH

/*
_update

Description:        If the data buffer needs to be updated, updates the buffer
                    and returns 1 (TRUE). Otherwise, returns 0 (FALSE).

Operation:          Checks if NeedBuffer flag is set. If it is, write the new
                    buffer address and size to Buffer2Address and Buffer2Size.
                    Then reset NeedBuffer. Otherwise, do nothing. Return the
                    original value of the NeedBuffer flag in R0.

Arguments:          buffer_adr  - R0 - address of the new buffer
                    buffer_size - R1 - size of the new buffer
Return Value:       need_buffer - R0 - 1 (TRUE) if the new buffer is used, 0 (FALSE)
                                       otherwise

Local Variables:    None.

Shared Variables:   Buffer2Address - address of the second buffer [W]
                    Buffer2Size    - size of the second buffer [W]
                    NeedBuffer     - set if a new buffer is needed [B]

Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16     Dennis Shim      initial revision
*/
_update:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R1;
    [--SP] = R2;

CheckNeedBuffer:                        // check NeedBuffer to see if we need a
                                        //   new buffer (return NeedBuffer)
    P0.L = NeedBuffer;                  // get NeedBuffer value
    P0.H = NeedBuffer;
    R2 = B[P0];                         // we will also return this value in R0

    CC = R2 == FALSE;                   // check if NeedBuffer is not set
    if CC   JUMP UpdateEnd;             // NeedBuffer is not set, finish
    //if !CC  JUMP UpdateBuffer;        // NeedBuffer is set, update the buffer

UpdateBuffer:                           // update buffer to buffer 2, and reset
                                        //   NeedBuffer
    P1.L = Buffer2Address;              // copy new buffer address to
    P1.H = Buffer2Address;              //   Buffer2Address
    [P1] = R0;

    P1.L = Buffer2Size;                 // copy new buffer size to Buffer2Size
    P1.H = Buffer2Size;
    [P1] = R1;

    R1 = FALSE;                         // prepare to reset NeedBuffer
    [P0] = R1;                          // reset NeedBuffer
    
UpdateEnd:
    R0 = R2;                            // this is our return value (NeedBuffer)

    R2 = [SP++];                        // restore registers
    R1 = [SP++];
    P1 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._update.END:

/*
_audio_play

Description:        Begins playing audio from the passed buffer. Turns on demand
                    interrupt (PF B).

Operation:          Assign Buffer1Address and Buffer1Size to the passed values.
                    Reset the BufferIndex to zero. Set the NeedBuffer flag.
                    Turns on the demand interrupt (PF B).

Arguments:          buffer_adr  - R0 - address of the new buffer
                    buffer_size - R1 - size of the new buffer
Return Value:       None.

Local Variables:    None.

Shared Variables:   Buffer1Address - address of the first buffer [W]
                    Buffer1Size    - size of the first buffer [W]
                    NeedBuffer     - set if a new buffer is needed [B]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/04/16     Dennis Shim      initial revision
*/
_audio_play:
    [--SP] = P0;                        // preserve registers
    [--SP] = R2;

InitializeVariables:                    // set Buffer1, set NeedBuffer, and
                                        //   BufferIndex
    P0.L = Buffer1Address;              // set the Buffer1Address
    P0.H = Buffer1Address;
    [P0] = R0;

    P0.L = Buffer1Size;                 // set the Buffer1Size
    P0.H = Buffer1Size;
    [P0] = R1;
    
    P0.L = BufferIndex;                 // zero the BufferIndex
    P0.H = BufferIndex;
    R2 = 0;
    [P0] = R2;

    P0.L = NeedBuffer;                  // set the NeedBuffer flag
    P0.H = NeedBuffer;
    R2 = TRUE;
    B[P0] = R2;

TurnOnDemand:
    // enable demand interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _enable_PFB;
    RETS = [SP++];                      // restore return address

AudioPlayEnd:
    R2 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._audio_play.END:

/*
_audio_halt

Description:        Stops playing audio.

Operation:          Disable SPI and demand interrupt.

Arguments:          None.
Return Value:       None.

Local Variables:    None.

Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/04/16     Dennis Shim      initial revision
*/
_audio_halt:
    
    // disable demand interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _disable_PFB;
    RETS = [SP++];                      // restore return address

    // disable SPI interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _disable_SPI;
    RETS = [SP++];                      // restore return address
    
    RTS;
    
._audio_halt.END:

/*
_elapsed_time

Description:        Returns the elapsed time in ms.

Operation:          Returns the shared variable ElapsedTime. Zero it.

Arguments:          None.
Return Value:       ElapsedTime - R0 - time elapsed since last time this function
                                       was called

Local Variables:    None.

Shared Variables:   ElapsedTime - time elapsed since last time this function was
                                  called [W]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/04/16     Dennis Shim      initial revision
*/
_elapsed_time:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    
    P0.L = ElapsedTime;                 // return ElapsedTime
    P0.H = ElapsedTime;
    
    R0 = [P0];

    R1 = 0;                             // reset ElapsedTime
    [P0] = R1;
    
    R1 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._elapsed_time.END:

/*
_increment_elapsed_time

Description:        Increments the elapsed time every ms.

Operation:          Increment ElapsedTime.

Arguments:          None.
Return Value:       None.

Local Variables:    None.

Shared Variables:   ElapsedTime - time elapsed since last time this function was
                                  called [W]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/04/16     Dennis Shim      initial revision
*/
_increment_elapsed_time:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    P0.L = ElapsedTime;                 // get ElapsedTime
    P0.H = ElapsedTime;
    
    R0 = [P0];
    
    R0 += 1;                            // increment ElapsedTime
    
    [P0] = R0;                          // write ElapsedTime
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._increment_elapsed_time.END:

/*
_SPI_init

Description:        Initializes SPI registers and enables SPI.

Operation:          Initialize SPI register values. Initialize NeedBuffer to zero
                    (FALSE). Initialize ElapsedTime to zero.

Arguments:          None.
Return Value:       None.

Local Variables:    None.

Shared Variables:   NeedBuffer  - set if a new buffer is needed [B]
                    ElapsedTime - time elapsed since last time this function was
                                  called [W]
Global Variables:   None.

Input:              None.
Output:             Writes to SPI registers.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16     Dennis Shim      initial revision
*/
_SPI_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Initialize SPI_BAUD register
    P0.L = SPI_BAUD_ADR;                 // get SPI_BAUD register address
    P0.H = SPI_BAUD_ADR;
    R0.L = SPI_BAUD_VAL;                 // get SPI_BAUD value

    W[P0] = R0.L;                       // write SPI_BAUD value to the address

    // Initialize SPI_CTL register
    P0.L = SPI_CTL_ADR;                 // get SPI_CTL register address
    P0.H = SPI_CTL_ADR;
    R0.L = SPI_CTL_VAL;                 // get SPI_CTL value
    
    W[P0] = R0.L;                       // write SPI_CTL value to the address

    // Initialize SPI_STAT register
    P0.L = SPI_STAT_ADR;                // get SPI_STAT register address
    P0.H = SPI_STAT_ADR;
    R0.L = SPI_STAT_VAL;                // get SPI_STAT value
    
    W[P0] = R0.L;                       // write SPI_STAT value to the address

    // Initialize NeedBuffer to FALSE
    P0.L = NeedBuffer;                  // prepare to write to NeedBuffer
    P0.H = NeedBuffer;
    R0 = FALSE;
    B[P0] = R0;                         // NeedBuffer is FALSE because we initially
                                        //   don't need a buffer (not playing music)
                                        
    // Initialize ElapsedTime to zero
    P0.L = ElapsedTime;                 // prepare to write to ElapsedTime
    P0.H = ElapsedTime;
    R0 = 0;
    [P0] = R0;                          // zero ElapsedTime because we initially
                                        //   have not had time elapsed
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._SPI_init.END:

/*
_SPI_handler

Description:        Event handler for the SPI interrupt. If demand is set, then
                    send a byte. Otherwise, disable the SPI interrupt, and
                    enable the demand interrupt.

Operation:          Push all registers. Check if the demand is set using the
                    get_demand function. If it is, then just send a byte and
                    return. If it is not set, then disable the SPI interrupt,
                    and enable the PF B interrupt. Pop all registers and return.

Arguments:          None.
Return Value:       None.

Local Variables:    None.

Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16     Dennis Shim      initial revision
*/
_SPI_handler:
    [--SP] = ASTAT;                     // preserve registers
    [--SP] = RETI;
    [--SP] = R0;

CheckDemandSPI:
    // get the demand in R0
    [--SP] = RETS;                      // preserve return address
    CALL _get_demand;
    RETS = [SP++];                      // restore return address

    CC = R0 == 0;                       // check if demand is not set
    //if CC   JUMP DisableSPI;          // demand is not set, stop SPI and enable
                                        //   demand interrupt
    if !CC  JUMP DemandEnabled;         // demand is set, send a byte

DisableSPI:                             // there was no demand, disable SPI
                                        //   interrupt and enable demand
    // disable SPI interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _disable_SPI;
    RETS = [SP++];                      // restore return address

    // enable demand interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _enable_PFB;
    RETS = [SP++];                      // restore return address

    JUMP SPIHandlerEnd;                 // and finish

DemandEnabled:                          // demand is still active, just send
                                        //   a byte
    // send a byte
    [--SP] = RETS;                      // preserve return address
    CALL _send_byte;
    RETS = [SP++];                      // restore return address

    //JUMP SPIHandlerEnd;               // and finish

SPIHandlerEnd:
    R0 = [SP++];                        // restore registers
    RETI = [SP++];                      
    ASTAT = [SP++];
    
    RTI;
    
._SPI_handler.END:

/*
_demand_handler

Description:        

Operation:          

Arguments:          None.
Return Value:       None.

Local Variables:    

Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    06/01/16     Dennis Shim      initial revision
*/
_demand_handler:

    // send a byte
    [--SP] = RETS;                      // preserve return address
    CALL _send_byte;
    RETS = [SP++];                      // restore return address

    // disable demand interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _disable_PFB;
    RETS = [SP++];                      // restore return address

    // enable SPI interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _enable_SPI;
    RETS = [SP++];                      // restore return address

    RTS;
    
._demand_handler.END:

/*
_send_byte

Description:        Sends a byte over SPI.

Operation:          Get a byte from Buffer 1 by reading from Buffer1Address +
                    BufferIndex. Increment BufferIndex. Check if it is less than
                    Buffer1Size. If it is, finish. Otherwise, copy Buffer2Address
                    to Buffer1Address, Buffer2Size to Buffer1Size, zero the
                    BufferIndex, and set the NeedBuffer flag.

Arguments:          None.
Return Value:       None.

Local Variables:    None.

Shared Variables:   Buffer1Address - address of the first buffer [W]
                    Buffer1Size    - size of the first buffer [W]
                    Buffer1Address - address of the second buffer [W]
                    Buffer1Size    - size of the second buffer [W]
                    BufferIndex    - index of the buffer
                    NeedBuffer     - set if a new buffer is needed [B]
Global Variables:   None.

Input:              None.
Output:             Outputs a byte over serial.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16     Dennis Shim      initial revision
*/
_send_byte:
    [--SP] = R0;                        // preserve registers
    [--SP] = R1;
    [--SP] = P0;
    [--SP] = P1;
    [--SP] = P2;
    [--SP] = P3;
    [--SP] = P4;
    
    P0.L = Buffer1Address;              // first read data from the buffer
    P0.H = Buffer1Address;
    R1 = [P0];
    
    P2.L = BufferIndex;                 // get the buffer index so we know which
    P2.H = BufferIndex;                 //   address to read from
    R0 = [P2];
    
    R1 = R1 + R0;
    P1 = R1;
    
    R1 = B[P1];                         // read a byte

    P4.L = SPI_TDBR_ADR;                // prepare to write the byte to transfer
    P4.H = SPI_TDBR_ADR;                //   it over SPI
    W[P4] = R1.L;
    
    R0 += 1;                            // increment the buffer index

    P3.L = Buffer1Size;                 // get the buffer size so we can compare
    P3.H = Buffer1Size;                 //   to the buffer index
    R1 = [P3];

    R1 <<= 1;                           // double buffer size b/c we sent bytes
    CC = R0 < R1;                       // check if index is less than size
    //if CC   JUMP StillMoreData;       // index is less than size, we still have data
    if !CC  JUMP ChangeBuffer;          // index has reached size, need to change buffers

StillMoreData:                          // still more data in the buffer, keep
                                        //   reading from the next index
    [P2] = R0;                          // update buffer index

    JUMP SendByteEnd;                   // finish

ChangeBuffer:                           // no more data in the buffer, read from
                                        //   buffer2 instead, reset index as well
    P0.L = Buffer2Address;              // prepare to copy the buffer 2 address
    P0.H = Buffer2Address;
    R0 = [P0];
    P0.L = Buffer1Address;
    P0.H = Buffer1Address;
    [P0] = R0;                          // copy buffer 2 address to buffer 1 address

    P0.L = Buffer2Size;                 // prepare to copy the buffer 2 size
    P0.H = Buffer2Size;
    R0 = [P0];
    P0.L = Buffer1Size;
    P0.H = Buffer1Size;
    [P0] = R0;                          // copy buffer 2 size to buffer 1 size

    P0.L = BufferIndex;                 // prepare to zero buffer index
    P0.H = BufferIndex;
    R0 = 0;
    [P0] = R0;                          // zero buffer index

    P0.L = NeedBuffer;                  // prepare to set the NeedBuffer flag
    P0.H = NeedBuffer;
    R0 = 1;
    [P0] = R0;                          // set the NeedBuffer flag

    //JUMP SendByteEnd;                 // and finish

SendByteEnd:
    P4 = [SP++];                        // restore registers
    P3 = [SP++];
    P2 = [SP++];
    P1 = [SP++];
    P0 = [SP++];
    R1 = [SP++];
    R0 = [SP++];
    
    RTS;
    
._send_byte.END:

/*
_install_SPI_EH

Description:        Installs the SPI interrupt event handler in the event
                    vector table.

Operation:          Installs the SPI event handler in the event vector table.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16   Dennis Shim      initial revision
*/
_install_SPI_EH:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    P0.L = EVT10_ADR;                   // get the address of EVT13
    P0.H = EVT10_ADR;
    R0.L = _SPI_handler;                // put the SPI event handler in EVT 10
    R0.H = _SPI_handler;

    [P0] = R0;
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._install_SPI_EH.END:

/*
_get_demand

Description:        Checks the status of the demand. Returns 0 if there is no
                    demand, non-zero otherwise in R0.

Operation:          Read from the PF register. Mask out all bits other than PF15, 
                    which is the demand flag. Return this in R0.

Arguments:          None.
Return Value:       demand - R0 - zero if there is no demand, nonzero if there is

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16   Dennis Shim      initial revision
*/
_get_demand:
    [--SP] = R1;                        // preserve registers
    
    // Get the PF registers
    [--SP] = RETS;                      // preserve return address
    CALL _get_PF;
    RETS = [SP++];                      // restore return address

    R1.L = KEEP_PF15_MASK;              // demand is PF15, only need to keep that
    R1.H = KEEP_PF15_MASK;
    R0 = R0 & R1;                       // mask out all other bits
    
    R1 = [SP++];                        // restore registers
    
    RTS;
    
._get_demand.END:

// Data segment
.section .data
ElapsedTime:                        // time elapsed in ms since last time elapsed_time
    .space 4                        //   was called
Buffer1Address:                     // address of the first buffer
    .space 4
Buffer1Size:                        // size of the first buffer, in bytes
    .space 4
Buffer2Address:                     // address of the second buffer
    .space 4
Buffer2Size:                        // size of the second buffer, in bytes
    .space 4
BufferIndex:                        // current index of the buffer
    .space 4
NeedBuffer:                         // 1 if buffer needed, 0 if unneeded
    .space 1

.end
