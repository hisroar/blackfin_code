//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                               display.s                                  //
//                           Display Functions                              //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for initializing and writing to the display.
    The functions included are:
        _display_time               - Displays the passed time. (PUBLIC)
        _display_status             - Displays the passed status. (PUBLIC)
        _display_title              - Displays the passed title. (PUBLIC)
        _display_artist             - Displays the passed artist. (PUBLIC) (UNUSED)
        _display_encoder            - Displays the passed menu status. Called
                                      when the encoder status changes. (PUBLIC)
        _display_init               - Initializes the display. (PUBLIC)
        _display_write_char         - Writes a character to the display. (PRIVATE)
        _display_write_string       - Writes a string to the display. (PRIVATE)
        _get_busy_flag              - Returns the busy flag. (PRIVATE)
        _display_send_signal        - Sends a signal to the display. (PRIVATE)
        _delay                      - Sits in a loop for a time. (PRIVATE)
    Revision History:
    05/07/16    Dennis Shim     initial revision
    06/14/16    Dennis Shim     tidied comments, fixed string issue
*/

.global _display_time
.global _display_status
.global _display_artist
.global _display_title
.global _display_init
.global _display_encoder

/* EXTERNS */
.extern _data_PF

/* Include constants from display.inc and keypad.inc and general.inc and pf.inc */
.include "src/sys/display.inc"
.include "src/sys/keypad.inc"
.include "src/sys/general.inc"
.include "src/sys/pf.inc"

/*
_display_time

Description:        Displays the passed time on the LCD display.

Operation:          Write the preface string time_string to the string_buffer.
                    Round the time to seconds by dividing by 10. Determine the
                    number of hours by dividing by SECONDS_IN_HOUR. Write the
                    quotient to the string_buffer using dec2string, and the
                    remainder of the division is the remaining seconds. Determine
                    the number of minutes by dividing by SECONDS_IN_MINUTE. Write
                    the quotient to the string_buffer using dec2string and then
                    write the remainder of the division (remaining seconds)
                    to the string_buffer using dec2string. Call _display_write_string
                    to write the string_buffer to the display.

Arguments:          time - R0 - time in tenths of a second. If value is TIME_NONE
                                no time should be displayed
Return Value:       None.

Local Variables:    time - R0,R2 - time remaining
                    str_adr - P0 - address of the string to write to
Shared Variables:   string_buffer - string of length STRING_BUFFER_LENGTH.
Global Variables:   None.

Input:              None.
Output:             Outputs time to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16    Dennis Shim      initial revision
*/
_display_time:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R4;
    [--SP] = R3;
    [--SP] = R2;
    [--SP] = R1;
    [--SP] = R0;
    
    P0.L = string_buffer;               // prepare to write to the string buffer
    P0.H = string_buffer;

    R1.L = STRING_BUFFER_LENGTH;        // prepare to clear the string buffer
    R1.H = STRING_BUFFER_LENGTH;

    // clear string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _clear_string;                 // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
    P1.L = time_string;                 // prepare to copy time_string to the
    P1.H = time_string;                 //   string buffer
    
    R1.L = PREFACE_STRING_LENGTH;       // pass PREFACE_STRING_LENGTH as the
    R1.H = PREFACE_STRING_LENGTH;       //   length argument to _copy_string
    
    // copy time_string into string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address

    R1 = 10;                            // round off the tenth of a second
    [--SP] = RETS;                      // preserve return address
    CALL _divide;                       // R2 contains quotient, R3 remainder
    RETS = [SP++];                      // restore return address
    R0 = R2;                            // put the quotient in R0

PrepWriteHours:
    R1 = SECONDS_IN_HOUR;               // prepare to get number of hours
    [--SP] = RETS;                      // preserve return address
    CALL _divide;                       // R2 contains quotient, R3 remainder
    RETS = [SP++];                      // restore return address

    // now check if we need to pad with a zero
    R4 = 10;
    CC = R2 < R4;                       // pad with zero if # hours is less than 10
    //if CC   JUMP PadZeroHours;        // # hours is less than 10, pad w/ zero
    if !CC  JUMP WriteHours;            // # hours is greater than 10, don't pad

PadZeroHours:
    R0.L = '0';                         // prepare to pad with a zero
    R0.H = '0';
    B[P0++] = R0;                       // write the "0", increment to next char

WriteHours:
    R0 = R2;                            // prepare to call dec2string with arguments
    R1 = P0;                            //   hours and str_adr

    [--SP] = RETS;                      // preserve return address
    CALL _dec2string;                   // write hours to the string
    RETS = [SP++];                      // restore return address

    P0 = R1;                            // update the string address to dec2string
                                        //   return value
    R0.L = ':';                         // write the colon after hours
    R0.H = ':';
    B[P0++] = R0;                       // increment the address

    R0 = R3;                            // remaining number of seconds is
                                        //   remainder from the divide

PrepWriteMinutes:
    R1 = SECONDS_IN_MINUTE;             // prepare to get number of minutes
    [--SP] = RETS;                      // preserve return address
    CALL _divide;                       // R2 contains quotient, R3 remainder
    RETS = [SP++];                      // restore return address

    // now check if we need to pad with a zero
    R4 = 10;
    CC = R2 < R4;                       // pad with zero if # mins is less than 10
    //if CC   JUMP PadZeroMinutes;      // # mins is less than 10, pad w/ zero
    if !CC  JUMP WriteMinutes;          // # mins is greater than 10, don't pad

PadZeroMinutes:
    R0.L = '0';                         // prepare to pad with a zero
    R0.H = '0';
    B[P0++] = R0;                       // write the "0", increment to next char

WriteMinutes:
    R0 = R2;                            // prepare to call dec2string with arguments
    R1 = P0;                            //   minutes and str_adr

    [--SP] = RETS;                      // preserve return address
    CALL _dec2string;                   // write minutes to the string
    RETS = [SP++];                      // restore return address

    P0 = R1;                            // update the string address to dec2string
                                        //   return value
    R0.L = ':';                         // write the colon after minutes
    R0.H = ':';
    B[P0++] = R0;                       // increment the address

PrepWriteSeconds:
    // check if we need to pad with a zero
    R4 = 10;
    CC = R3 < R4;                       // pad with zero if # seconds is less than 10
    //if CC   JUMP PadZeroSeconds;      // # seconds is less than 10, pad w/ zero
    if !CC  JUMP WriteSeconds;          // # seconds is greater than 10, don't pad

PadZeroSeconds:
    R0.L = '0';                         // prepare to pad with a zero
    R0.H = '0';
    B[P0++] = R0;                       // write the "0", increment to next char

WriteSeconds:
    R0 = R3;                            // prepare to call dec2string with arguments
    R1 = P0;                            //   minutes and str_adr

    [--SP] = RETS;                      // preserve return address
    CALL _dec2string;                   // write minutes to the string
    RETS = [SP++];                      // restore return address

WriteTimeString:                        // write the time string to display
    R0.L = string_buffer;               // pass string_buffer and display address
    R0.H = string_buffer;
    R1.L = TIME_ADR;
    R1.H = TIME_ADR;

    [--SP] = RETS;                      // preserve return address
    CALL _display_write_string;
    RETS = [SP++];                      // restore return address

    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    R2 = [SP++];
    R3 = [SP++];
    R4 = [SP++];
    P1 = [SP++];
    P0 = [SP++];

    RTS;

._display_time.END:

/*
_display_status

Description:        Writes the status to the display. If STATUS_PLAY is passed,
                    "Play" will be displayed. If STATUS_FASTFWD is passed, "Fast
                    Forward" will be displayed. If STATUS_REVERSE is passed,
                    "Reverse" will be displayed. If STATUS_IDLE is passed, "Idle"
                    will be displayed.

Operation:          Write the status_string to the string_buffer. Check which
                    status was passed using if statements. Write the correct
                    message depending on the status passed to string_buffer.
                    Use _display_write_string to write string_buffer to the
                    display.

Arguments:          status - R0 - status code
Return Value:       None.

Local Variables:    None.
Shared Variables:   string_buffer - string of length STRING_BUFFER_LENGTH.
Global Variables:   None.

Input:              None.
Output:             Outputs status to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16    Dennis Shim      initial revision
*/
_display_status:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R2;
    [--SP] = R1;
    [--SP] = R0;
    
    P0.L = string_buffer;               // prepare to write to the string buffer
    P0.H = string_buffer;

    R1.L = STRING_BUFFER_LENGTH;        // prepare to clear the string buffer
    R1.H = STRING_BUFFER_LENGTH;

    // clear string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _clear_string;                 // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
    P1.L = status_string;               // prepare to copy status_string to the
    P1.H = status_string;               //   string buffer
    
    R1.L = PREFACE_STRING_LENGTH;       // pass PREFACE_STRING_LENGTH as the
    R1.H = PREFACE_STRING_LENGTH;       //   length argument to _copy_string
    
    // copy time_string into string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address

CheckWhichStatus:
    R1.L = STATUS_STRING_LENGTH;        // prepare to pass STATUS_STRING_LENGTH
    R1.H = STATUS_STRING_LENGTH;        //   as an argument to _copy_string

    R2.L = STATUS_PLAY;
    R2.H = STATUS_PLAY;
    CC = R0 == R2;                      // check if the status is STATUS_PLAY
    if CC   JUMP StatusPlay;            // it is, write the string "Play"
    
    R2.L = STATUS_FASTFWD;
    R2.H = STATUS_FASTFWD;
    CC = R0 == R2;                      // check if the status is STATUS_FASTFWD
    if CC   JUMP StatusFastFwd;         // it is, write the string "Fast Forward"
    
    R2.L = STATUS_REVERSE;
    R2.H = STATUS_REVERSE;
    CC = R0 == R2;                      // check if the status is STATUS_REVERSE
    if CC   JUMP StatusReverse;         // it is, write the string "Reverse"

    R2.L = STATUS_REVERSE;
    R2.H = STATUS_REVERSE;
    CC = R0 == R2;                      // check if the status is STATUS_REVERSE
    if CC   JUMP StatusReverse;         // it is, write the string "Reverse"
    
    R2.L = STATUS_IDLE;
    R2.H = STATUS_IDLE;
    CC = R0 == R2;                      // check if the status is STATUS_IDLE
    if CC   JUMP StatusIdle;            // it is, write the string "Idle"
    
    JUMP WriteStatusString;             // should never get here, if we do only
                                        //   write previous
    
StatusPlay:                             // make the status_string play_string
    P1.L = play_string;
    P1.H = play_string;
    
    JUMP CopyStatusString;              // now copy the status string

StatusFastFwd:                          // make the status_string fastfwd_string
    P1.L = fastfwd_string;
    P1.H = fastfwd_string;
    
    JUMP CopyStatusString;              // now copy the status string

StatusReverse:                          // make the status_string reverse_string
    P1.L = reverse_string;
    P1.H = reverse_string;
    
    JUMP CopyStatusString;              // now copy the status string

StatusIdle:                             // make the status_string idle_string
    P1.L = idle_string;
    P1.H = idle_string;
    
    JUMP CopyStatusString;              // now copy the status string

CopyStatusString:                       // just copy the status string to the
                                        //   string buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
WriteStatusString:                      // use _display_write_string to output
                                        //   the status to the display
    R0.L = string_buffer;               // pass string_buffer and display address
    R0.H = string_buffer;
    R1.L = STATUS_ADR;
    R1.H = STATUS_ADR;

    [--SP] = RETS;                      // preserve return address
    CALL _display_write_string;
    RETS = [SP++];                      // restore return address
    
EndDisplayStatus:
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    R2 = [SP++];
    P1 = [SP++];
    P0 = [SP++];

    RTS;

._display_status.END:

/*
_display_encoder

Description:        Writes the status of the encoder to the display. If
                    KEY_TRACKUP is passed, "Track Up" will be displayed. If
                    KEY_TRACKDOWN is passed, "Track Down" will be displayed.
                    If KEY_PLAY is passed, "Play" will be displayed. If 
                    KEY_RPTPLAY is passed, "Repeat" will be displayed. If
                    KEY_FASTFWD is passed, "Fast Forward" will be displayed. If
                    KEY_REVERSE is passed, "Reverse" will be displayed. If 
                    KEY_STOP is passed, "Stop will be displayed".

Operation:          Write the encoder_string to the string_buffer. Check which
                    key was passed using if statements. Write the correct
                    message depending on the key passed to string_buffer.
                    Use _display_write_string to write string_buffer to the
                    display.

Arguments:          status - R0 - key code
Return Value:       None.

Local Variables:    None.
Shared Variables:   string_buffer - string of length STRING_BUFFER_LENGTH.
Global Variables:   None.

Input:              None.
Output:             Outputs status to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16    Dennis Shim     initial revision
    06/14/16    Dennis Shim     fixed repeat/reverse strings
*/
_display_encoder:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R2;
    [--SP] = R1;
    [--SP] = R0;
    
    R2 = R0;                            // move R0 to R2 to compare
    
    P0.L = string_buffer;               // prepare to write to the string buffer
    P0.H = string_buffer;

    R1.L = STRING_BUFFER_LENGTH;        // prepare to clear the string buffer
    R1.H = STRING_BUFFER_LENGTH;

    // clear string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _clear_string;                 // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
    P1.L = encoder_string;              // prepare to copy encoder_string to the
    P1.H = encoder_string;              //   string buffer
    
    R1.L = PREFACE_STRING_LENGTH;       // pass PREFACE_STRING_LENGTH as the
    R1.H = PREFACE_STRING_LENGTH;       //   length argument to _copy_string
    
    // copy time_string into string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address

CheckWhichKey:
    R1.L = STATUS_STRING_LENGTH;        // prepare to pass STATUS_STRING_LENGTH
    R1.H = STATUS_STRING_LENGTH;        //   as an argument to _copy_string

    R2.L = KEY_TRACKUP;
    R2.H = KEY_TRACKUP;
    CC = R0 == R2;                      // check if the status is KEY_TRACKUP
    if CC   JUMP KeyTrackUp;            // it is, write the string "Track Up"

    R2.L = KEY_TRACKDOWN;
    R2.H = KEY_TRACKDOWN;
    CC = R0 == R2;                      // check if the status is KEY_TRACKDOWN
    if CC   JUMP KeyTrackDown;          // it is, write the string "Track Down"

    R2.L = KEY_PLAY;
    R2.H = KEY_PLAY;
    CC = R0 == R2;                      // check if the status is KEY_PLAY
    if CC   JUMP KeyPlay;               // it is, write the string "Play"

    R2.L = KEY_RPTPLAY;
    R2.H = KEY_RPTPLAY;
    CC = R0 == R2;                      // check if the status is KEY_RPTPLAY
    if CC   JUMP KeyRepeat;             // it is, write the string "Repeat"
    
    R2.L = KEY_FASTFWD;
    R2.H = KEY_FASTFWD;
    CC = R0 == R2;                      // check if the status is KEY_FASTFWD
    if CC   JUMP KeyFastFwd;            // it is, write the string "Fast Forward"
    
    R2.L = KEY_REVERSE;
    R2.H = KEY_REVERSE;
    CC = R0 == R2;                      // check if the status is KEY_REVERSE
    if CC   JUMP KeyReverse;            // it is, write the string "Reverse"
    
    R2.L = KEY_STOP;
    R2.H = KEY_STOP;
    CC = R0 == R2;                      // check if the status is KEY_STOP
    if CC   JUMP KeyStop;               // it is, write the string "Stop"
    
    JUMP WriteKeyString;                // should never get here, if we do only
                                        //   write previous

KeyTrackUp:                             // make the string trackup_string
    P1.L = trackup_string;
    P1.H = trackup_string;
    
    JUMP CopyKeyString;                 // now copy the key string


KeyTrackDown:                           // make the string trackdown_string
    P1.L = trackdown_string;
    P1.H = trackdown_string;
    
    JUMP CopyKeyString;                 // now copy the key string

KeyPlay:                                // make the status_string play_string
    P1.L = play_string;
    P1.H = play_string;
    
    JUMP CopyKeyString;                 // now copy the key string

KeyRepeat:                              // make the string repeat_string
    P1.L = repeat_string;
    P1.H = repeat_string;
    
    JUMP CopyKeyString;                 // now copy the key string

KeyFastFwd:                             // make the status_string fastfwd_string
    P1.L = fastfwd_string;
    P1.H = fastfwd_string;
    
    JUMP CopyKeyString;                 // now copy the key string

KeyReverse:                             // make the string reverse_string
    P1.L = reverse_string;
    P1.H = reverse_string;
    
    JUMP CopyKeyString;                 // now copy the key string

KeyStop:                                // make the string stop_string
    P1.L = stop_string;
    P1.H = stop_string;
    
    JUMP CopyKeyString;                 // now copy the key string

CopyKeyString:                          // just copy the key string to the
                                        //   string buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
WriteKeyString:                         // use _display_write_string to output
                                        //   the encoder status to the display
    R0.L = string_buffer;               // pass string_buffer and display address
    R0.H = string_buffer;
    R1.L = ENCODER_ADR;
    R1.H = ENCODER_ADR;

    [--SP] = RETS;                      // preserve return address
    CALL _display_write_string;
    RETS = [SP++];                      // restore return address
    
EndDisplayEncoder:
    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    R2 = [SP++];
    P1 = [SP++];
    P0 = [SP++];

    RTS;

._display_encoder.END:

/*
_display_title

Description:        Writes the track title to the display. The address of the
                    track title is passed as an argument in R0.

Operation:          Copy title_string to string_buffer. Copy the title name
                    to the string_buffer. Write title to the display using
                    _display_write_string.

Arguments:          title_name - R0 - address of the track title
Return Value:       None.

Local Variables:    None.
Shared Variables:   string_buffer - string of length STRING_BUFFER_LENGTH.
Global Variables:   None.

Input:              None.
Output:             Outputs the title to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16    Dennis Shim      initial revision
*/
_display_title:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R2;
    [--SP] = R1;
    
    P0.L = string_buffer;               // prepare to write to the string buffer
    P0.H = string_buffer;
    
    R1.L = STRING_BUFFER_LENGTH;        // prepare to clear the string buffer
    R1.H = STRING_BUFFER_LENGTH;

    // clear string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _clear_string;                 // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
    P1.L = title_string;                // prepare to copy title_string to the
    P1.H = title_string;                //   string buffer
    
    R1.L = PREFACE_STRING_LENGTH;       // pass PREFACE_STRING_LENGTH as the
    R1.H = PREFACE_STRING_LENGTH;       //   length argument to _copy_string
    
    // copy time_string into string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address

CopyTitle:
    P1 = R0;                            // prepare to copy the string of the
                                        //   title to the string_buffer
    R1.L = DISPLAY_LINE_LENGTH;         // we don't want to write past the line
    R1.H = DISPLAY_LINE_LENGTH;         //   so pass DISPLAY_LINE_LENGTH - PREFACE_STRING_LENGTH +1
    R2.L = PREFACE_STRING_LENGTH;
    R2.H = PREFACE_STRING_LENGTH;
    
    R1 = R1 - R2;
    
    R2 = 1;
    R1 = R1 + R2;
    
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
WriteTitleString:                       // use _display_write_string to output
                                        //   the title to the display
    R0.L = string_buffer;               // pass string_buffer and display address
    R0.H = string_buffer;
    R1.L = TITLE_ADR;
    R1.H = TITLE_ADR;

    [--SP] = RETS;                      // preserve return address
    CALL _display_write_string;
    RETS = [SP++];                      // restore return address
    
EndDisplayTitle:
    R1 = [SP++];                        // restore registers
    R2 = [SP++];
    P1 = [SP++];
    P0 = [SP++];

    RTS;

._display_title.END:

/*
_display_artist

Description:        Writes the track artist to the display. The address of the
                    track artist is passed as an argument in R0. (UNUSED)

Operation:          Copy artist_string to string_buffer. Copy the artist name
                    to the string_buffer. Write artist to the display using
                    _display_write_string.

Arguments:          artist_name - R0 - address of the track artist
Return Value:       None.

Local Variables:    None.
Shared Variables:   string_buffer - string of length STRING_BUFFER_LENGTH.
Global Variables:   None.

Input:              None.
Output:             Outputs the artist to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16    Dennis Shim      initial revision
*/
_display_artist:
/*
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R2;
    [--SP] = R1;
    
    P0.L = string_buffer;               // prepare to write to the string buffer
    P0.H = string_buffer;

    R1.L = STRING_BUFFER_LENGTH;        // prepare to clear the string buffer
    R1.H = STRING_BUFFER_LENGTH;

    // clear string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _clear_string;                 // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
    P1.L = artist_string;               // prepare to copy artist_string to the
    P1.H = artist_string;               //   string buffer
    
    R1.L = PREFACE_STRING_LENGTH;       // pass PREFACE_STRING_LENGTH as the
    R1.H = PREFACE_STRING_LENGTH;       //   length argument to _copy_string
    
    // copy time_string into string_buffer
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address

CopyArtist:
    P1 = R0;                            // prepare to copy the string of the
                                        //   artist to the string_buffer
    R1.L = DISPLAY_LINE_LENGTH;         // we don't want to write past the line
    R1.H = DISPLAY_LINE_LENGTH;         //   so pass DISPLAY_LINE_LENGTH - PREFACE_STRING_LENGTH
    R2.L = PREFACE_STRING_LENGTH;
    R2.H = PREFACE_STRING_LENGTH;
    
    R1 = R1 - R2;
    
    [--SP] = RETS;                      // preserve return address
    CALL _copy_string;                  // P0 now contains the next address of
                                        //   string buffer to write to
    RETS = [SP++];                      // restore return address
    
WriteArtistString:                       // use _display_write_string to output
                                        //   the artist to the display
    R0.L = string_buffer;               // pass string_buffer and display address
    R0.H = string_buffer;
    R1.L = ARTIST_ADR;
    R1.H = ARTIST_ADR;

    [--SP] = RETS;                      // preserve return address
    CALL _display_write_string;
    RETS = [SP++];                      // restore return address
    
EndDisplayArtist:
    R1 = [SP++];                        // restore registers
    R2 = [SP++];
    P1 = [SP++];
    P0 = [SP++];
*/
    RTS;

._display_artist.END:



/*
_display_init

Description:        Initialize the Blackfin LCD display.

Operation:          Send the DISPLAY_WAKE_UP command three times with the
                    appropriate time delays between each. Finish the display
                    initialization by sending DISPLAY_FUNCTION_SET, DISPLAY_CURSOR_SET,
                    DISPLAY_CONTROL, and DISPLAY_ENTRY_MODE.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             PFs were changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_display_init:
    [--SP] = R0;                        // preserve registers
    [--SP] = R1;

    R0 = 0;                             // zero PFs initially
    [--SP] = RETS;                      // preserve return address
    CALL _data_PF;
    RETS = [SP++];                      // restore return address

    R0.L = DELAY_POWER;                 // delay wake up signals for >40 ms 
    R0.H = DELAY_POWER;                 //   after power is applied
    // call the delay function with DELAY_POWER as the argument
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address

InitWakeUp1:
    // use _display_send_signal to initialize the display
    R0.L = DISPLAY_WAKE_UP;             // prepare to send the next wake up signal
    R0.H = DISPLAY_WAKE_UP;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    // delay b/c busy flag isn't ready
    R0.L = DELAY_WAKE_UP_1;             // delay next wake up signal for 5 ms
    R0.H = DELAY_WAKE_UP_1;
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address

InitWakeUp2:
    R0.L = DISPLAY_WAKE_UP;             // prepare to send the next wake up signal
    R0.H = DISPLAY_WAKE_UP;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    // delay b/c busy flag isn't ready
    R0.L = DELAY_WAKE_UP_2;             // delay next wake up signal for 160 µs
    R0.H = DELAY_WAKE_UP_2;
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address

InitWakeUp3:
    R0.L = DISPLAY_WAKE_UP;             // prepare to send the next wake up signal
    R0.H = DISPLAY_WAKE_UP;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    // delay b/c busy flag isn't ready
    R0.L = DELAY_WAKE_UP_2;             // delay next wake up signal for 160 µs
    R0.H = DELAY_WAKE_UP_2;
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address

FinishInit:
    R0.L = DISPLAY_FUNCTION_SET;        // set display to 8-bit, 2-lines
    R0.H = DISPLAY_FUNCTION_SET;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address
    
    R0.L = DISPLAY_CURSOR_SET;          // set cursor moving
    R0.H = DISPLAY_CURSOR_SET;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    R0.L = DISPLAY_CONTROL;             // display ON, cursor ON, blinking ON
    R0.H = DISPLAY_CONTROL;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    R0.L = DISPLAY_ENTRY_MODE;          // set entry mode
    R0.H = DISPLAY_ENTRY_MODE;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

                                        // clear the display
    [--SP] = RETS;                      // preserve return address
    CALL _display_clear;
    RETS = [SP++];                      // restore return address
    
                                        // return DDRAM address to 0
    [--SP] = RETS;                      // preserve return address
    CALL _display_home;
    RETS = [SP++];                      // restore return address
    
    // delay b/c busy flag isn't ready
    R0.L = DELAY_WAKE_UP_2;             // delay next wake up signal for 160 µs
    R0.H = DELAY_WAKE_UP_2;
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address
    
    R0.L = FIRST_KEY;                   // initialize the encoder menu to FIRST_KEY
    R0.H = FIRST_KEY;
    [--SP] = RETS;                      // preserve return address
    CALL _display_encoder;
    RETS = [SP++];                      // restore return address

    R1 = [SP++];                        // restore registers
    R0 = [SP++];

	RTS;

._display_init.END:

/*
_display_clear

Description:        Clears the display.

Operation:          Send DISPLAY_CLEAR over PFs.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             PFs were changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_display_clear:
    [--SP] = R0;                        // preserve registers

    R0.L = DISPLAY_CLEAR;               // clear the display
    R0.H = DISPLAY_CLEAR;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    R0 = [SP++];                        // restore registers

	RTS;

._display_clear.END:

/*
_display_home

Description:        Moves DDRAM address of display to zero.

Operation:          Send DISPLAY_RET_HOME over PFs.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             PFs were changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_display_home:
    [--SP] = R0;                        // preserve registers

    R0.L = DISPLAY_RET_HOME;            // set DDRAM address to 00H
    R0.H = DISPLAY_RET_HOME;
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    R0 = [SP++];                        // restore registers

	RTS;

._display_home.END:

/*
_display_write_char

Description:        Writes a character to the display.

Operation:          Write the character using _display_send_signal. 

Arguments:          char - R0 - character to write to the display.
Return Value:       None.

Local Variables:    mask_temp - R1 - contains mask for address or data sending
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Writes a character to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16    Dennis Shim     initial revision
*/
_display_write_char:
    [--SP] = R0;                        // preserve registers
    [--SP] = R1;

    R1.L = DISPLAY_WRITE_DATA;          // prepare to write data
    R1.H = DISPLAY_WRITE_DATA;
    R0 = R0 | R1;                       // fill the data bits so display knows
                                        //   that we want to send data
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address

    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    
    RTS;
    
._display_write_char.END:

/*
_display_write_string

Description:        Writes a string to the display.

Operation:          Loop through the string until the null character is reached.
                    Check if the busy flag is set. If it is, block. If it isn't,
                    write a character to the display. Also make sure the string
                    does not go to the next line.

Arguments:          disp_adr - R1 - address on the display to write first char to.
                    str_adr  - R0 - address of the string to display.
Return Value:       None.

Local Variables:    str_len  - R4 - length of the string
Shared Variables:   None.
Global Variables:   None.

Input:              Reads busy flag from display.
Output:             Writes a string to the display.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16    Dennis Shim     initial revision
    06/10/16    Dennis Shim     changed how DDRAM address is set
*/
_display_write_string:
    [--SP] = P0;                        // preserve registers
    [--SP] = R4;
    [--SP] = R3;
    [--SP] = R2;
    [--SP] = R1;
    [--SP] = R0;

    P0 = R0;                            // put the address of the string in P0
    R4 = 0;                             // no lines have been read yet
    
WriteDispAdr:                           // since we have a char to write, specify
                                        //   the display address to write to
    R2.L = DISPLAY_DDRAM_ADR_SET;       // prepare to write address
    R2.H = DISPLAY_DDRAM_ADR_SET;
    R0 = R1 | R2;                       // fill the address bit so display knows
                                        //   that we want to set address, and
                                        //   we need to pass the argument in R0
    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address
    
CheckNull:                              // check if the null character was reached
    R3 = B[P0];                         // get the character at P0
    CC = R3 == 0;                       // check if the character is the null char
    if CC   JUMP EndWriteString;        // char is null char, stop looping
    //if !CC  JUMP CheckLine;           // char is not null, check if we have
                                        //   reached the end of line yet

CheckLine:                              
    R5.L = DISPLAY_LINE_LENGTH;
    R5.H = DISPLAY_LINE_LENGTH;
    CC = R4 < R5                        // check if the string has reached the end
                                        //   of the display line
    //if CC   JUMP WriteDispAdr         // has not reached end of line, keep writing
    if !CC  JUMP EndWriteString         // reached end of line, stop writing

CheckBusyFlag:                          // check if the busy flag is set, if it
                                        //   is block, if it isn't write the
                                        //   character to display                             
    [--SP] = RETS;                      // preserve return address
    CALL _get_busy_flag;                // busy flag in R0
    RETS = [SP++];                      // restore return address

    CC = R0 == 0;                       // check if the busy flag is not set
    //if CC   JUMP SendChar;            // busy flag is not set, send a char
    if !CC  JUMP CheckBusyFlag;         // busy flag is set, block

SendChar:                               // send a character to display using
                                        //   _display_write_char
    R0 = R3;                            // pass the character in R0
    [--SP] = RETS;                      // preserve return address
    CALL _display_write_char;
    RETS = [SP++];                      // restore return address

StringLoopEnd:                          // take care of rest of business
    P0 += 1;                            // prepare to get the next char
    //R1 += 1;                          // write to the next display address
    R4 += 1;                            // we have written another character
    JUMP CheckNull;

EndWriteString:
    // return DDRAM address to 0
    [--SP] = RETS;                      // preserve return address
    CALL _display_home;
    RETS = [SP++];                      // restore return address

    R0 = [SP++];                        // restore registers
    R1 = [SP++];
    R2 = [SP++];
    R3 = [SP++];
    R4 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._display_write_string.END:

/*
_get_busy_flag

Description:        Returns the busy flag.

Operation:          First change DB7:0 to inputs. Then change R/W to 1 to read
                    the busy flag.

Arguments:          None.
Return Value:       busy_flag - R0 - 1 if the display cannot be written to, 0
                                     otherwise

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              Reads busy flag from display.
Output:             Changes the FIO_DIR register.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16     Dennis Shim      initial revision
*/
_get_busy_flag:
    [--SP] = P0;                        // preserve registers
    [--SP] = R1;
    
    // make PF7:0 inputs so we can read from the display
    P0.L = FIO_DIR_ADR;                 // get FIO_DIR register address
    P0.H = FIO_DIR_ADR;
    R0.L = FIO_DIR_INP;                 // get FIO_DIR value to write
    W[P0] = R0.L;                       // write FIO_DIR value to the address

    R0.L = DISPLAY_BUSY_FLAG_READ;      // prepare to read busy flag
    R0.H = DISPLAY_BUSY_FLAG_READ;

    [--SP] = RETS;                      // preserve return address
    CALL _display_send_signal;
    RETS = [SP++];                      // restore return address
    
    R0.L = DELAY_BUSY;                  // need to delay before getting busy flag
    R0.H = DELAY_BUSY;
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address
    
    // now we need to read the busy flag
    [--SP] = RETS;                      // preserve return address
    CALL _get_PF;
    RETS = [SP++];                      // restore return address
    
    // R0.L now contains the PFs
    R0.H = 0;                           // clear junk out of high bit
    R0 >>= 0x07;                        // shift R0 so that only top bit of R0.L
                                        //   remains -- this is our busy flag
    R1.L = LOWER_BIT_MASK;              // only keep the lower bit
    R1.H = LOWER_BIT_MASK;
    R0 = R0 & R1;
                                        
    [--SP] = R0;                        // preserve return value
    
    // make PF7:0 outputs again
    P0.L = FIO_DIR_ADR;                 // get FIO_DIR register address
    P0.H = FIO_DIR_ADR;
    R0.L = FIO_DIR_VAL;                 // get FIO_DIR value to write
    W[P0] = R0.L;                       // write FIO_DIR value to the address
    
    R0 = [SP++];                        // restore return value

    R1 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._get_busy_flag.END:

/*
_display_send_signal

Description:        Sends a signal in R0 to the display.

Operation:          Use the argument R0. OR it with DISPLAY_E_ENABLE to have
                    E one initially and write it to PFs. Then delay for E's
                    pulse width. Then OR the argument with DISPLAY_E_DISABLE
                    to have E zero to get the falling edge and write to to PFs.

Arguments:          PF_val - R0 - value to send through PFs to display.
Return Value:       None.

Local Variables:    mask_temp - R1 - contains mask for E disable/enable
                    PF_val_temp - R0 - contains value OR'd with E disable/enable
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Writes to the PF register.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16     Dennis Shim      initial revision
*/
_display_send_signal:
    [--SP] = R0;                        // preserve registers
    [--SP] = R1;

    // first OR argument to make E = 1
    R1.L = DISPLAY_E_ENABLE;            // start with E set to 1
    R1.H = DISPLAY_E_ENABLE;
    R0 = R0 | R1;
    // now send the PF values
    [--SP] = RETS;                      // preserve return address
    CALL _data_PF;
    RETS = [SP++];                      // restore return address

    [--SP] = R0;                        // preserve argument

    // delay for E's pulse width (>300 ns)
    R0.L = DELAY_COMMAND;               // delay wake up signal for 300 ns
    R0.H = DELAY_COMMAND
    [--SP] = RETS;                      // preserve return address
    CALL _delay;
    RETS = [SP++];                      // restore return address

    R0 = [SP++];                        // restore argument

    // now AND argument to make E = 0
    R1.L = DISPLAY_E_DISABLE;           // now we need to set E to 0 to send data
    R1.H = DISPLAY_E_DISABLE;
    R0 = R0 & R1;
    // now send the PF values again
    [--SP] = RETS;                      // preserve return address
    CALL _data_PF;
    RETS = [SP++];                      // restore return address

    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    
    RTS;
    
._display_send_signal.END:

/*
_delay

Description:        Loops for the number of times specified in R0.

Operation:          Loop for the number of times specified in R0.

Arguments:          times - R0 - number of times we should loop.
Return Value:       None.

Local Variables:    count - R1 - number of times we have looped.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/07/16     Dennis Shim      initial revision
*/
_delay:
    [--SP] = R1;                        // preserve registers
    R1 = 0;                             // start count at zero and increment

LoopCondition:                          // check if count has reached times
    CC = R1 < R0;
    //if CC   JUMP LoopBody;            // count is still less than times
    if !CC  JUMP EndDelay               // count has reached times, stop looping

LoopBody:
    R1 += 1;                            // increment R1
    JUMP LoopCondition;                 // loop again

EndDelay:
    R1 = [SP++];                        // restore registers
    
    RTS;
    
._delay.END:

/*
_copy_string

Description:        Copies a string given its length into a buffer (assumes that
                    the buffer's length is longer than the string's). The string
                    buffer is null terminated. The address of the NULL char is
                    returned in P0.

Operation:          Check if the null character has been reached. Check if the
                    max length has been reached. Loop through each character
                    and copy it into the string buffer that was passed.

Arguments:          str_buf - P0 - buffer to copy the string to
                    string  - P1 - string to copy to the buffer
                    length  - R1 - length of the string to copy
Return Value:       None.

Local Variables:    count - R0 - number of chars we have written
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16     Dennis Shim      initial revision
*/
_copy_string:
    [--SP] = R0;                        // preserve registers
    [--SP] = R2;
    [--SP] = R3;
    [--SP] = P1;

    R0 = 0;                             // use R0 as a counter for how many chars
                                        //   of the string we've written
    
StringCopyNullCondition:                // check if we've reached a null character
    R2 = B[P1++];                       // get the character of string and
                                        //   increment address to next char
    R3.L = ASCII_NULL;
    R3.H = ASCII_NULL;
    CC = R2 == R3;                      // check if the current char is NULL
    if CC   JUMP EndStringCopy          // it was, stop copying
    //if !CC  JUMP StringCopyLengthCondition// it wasn't, check length

StringCopyLengthCondition:              // check if we are still within the max
                                        //   length we are allowed
    CC = R0 < R1;                       // check if we've reached the end of the
                                        //   string to copy
    //if CC  JUMP StringCopyBody;       // we haven't, keep writing the string
    if !CC  JUMP EndStringCopy;         // we have, stop writing the string
    
StringCopyBody:                         // write a character in the string to be
                                        //    copied to the string buffer
    B[P0++] = R2;                       // write character of the string to
                                        //   str_buf and increment address to
                                        //   next char
    R0 += 1;                            // increment our counter to indicate that
                                        //   we've gone to the next character
    JUMP StringCopyNullCondition;       // check if we should keep looping

EndStringCopy:/*
    R2.L = ASCII_NULL;                  // null terminate the string
    R2.H = ASCII_NULL;
    B[P0] = R2;
    */  
    
    P1 = [SP++];                        // restore registers
    R3 = [SP++];
    R2 = [SP++];
    R0 = [SP++];
    
    RTS;
    
._copy_string.END:

/*
_clear_string

Description:        Put spaces until the end of a string_buffer. The end must
                    be specified.

Operation:          Loops through the string buffer. Fills each position with
                    a space. Ends the buffer with a ASCII_NULL

Arguments:          str_buf - P0 - buffer to copy the string to
                    length  - R1 - length of the string to copy
Return Value:       None.

Local Variables:    count - R0 - number of chars we have written
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/08/16     Dennis Shim      initial revision
*/
_clear_string:
    [--SP] = R0;                        // preserve registers
    [--SP] = R2;
    [--SP] = P0;
    
    R0 = 1;
    R1 = R1 - R0;
    
    R0 = 0;                             // use R0 as a counter for how many chars
                                        //   of the string we've written
    R2.L = ' ';                         // use R2 to write to the string buffer
    R2.H = ' ';
    
StringClearLengthCondition:             // check if we are still within the max
                                        //   length we are allowed
    CC = R0 < R1;                       // check if we've reached the end of the
                                        //   string to clear
    //if CC  JUMP StringCopyBody;       // we haven't, keep writing the string
    if !CC  JUMP EndStringClear;        // we have, stop writing the string
    
StringClearBody:                        // write a character in the string to be
                                        //    copied to the string buffer
    B[P0++] = R2;                       // write NULL CHAR to str_buf and
                                        //   increment address to next char
    R0 += 1;                            // increment our counter to indicate that
                                        //   we've gone to the next character
    JUMP StringClearLengthCondition;    // check if we should keep looping

EndStringClear:
    R2.L = ASCII_NULL;                  // null terminate the string
    R2.H = ASCII_NULL;
    B[P0] = R2;
    
    P0 = [SP++];                        // restore registers
    R2 = [SP++];
    R0 = [SP++];
    
    RTS;
    
._clear_string.END:

// Data segment
.section .data
string_buffer:                      // string of length STRING_BUFFER_LENGTH
    .space STRING_BUFFER_LENGTH
time_string:                        // string to preface time 
    .string "Time: "
status_string:                      // string to preface status
    .string "Status: "
title_string:                       // string to preface title
    .string "Title: "
encoder_string:                     // string to preface encoder
    .string "Menu: "
artist_string:                      // string to preface artist
    .string "Artist: "
play_string:                        // string if status is STATUS_PLAY or ksy is KEY_PLAY
    .string "Play"
fastfwd_string:                     // string if status is STATUS_FASTFWD or key is KEY_FASTFWD
    .string "Fast Forward"
reverse_string:                     // string if status is STATUS_REVERSE
    .string "Reverse"
idle_string:                        // string if status is STATUS_IDLE
    .string "Idle"
trackup_string:                     // string if status is KEY_TRACKUP
    .string "Track Up"
trackdown_string:                   // string if status is KEY_TRACKDOWN
    .string "Track Down"
repeat_string:                      // string if status is KEY_RPTPLAY
    .string "Repeat"
stop_string:                        // string if status is KEY_STOP
    .string "Stop"
.end
