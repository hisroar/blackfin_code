Documentation for the Blackfin MP3 player can be found in the technical manual
(tman.pdf) and the user manual (uman.pdf).

-------------------------------------------------------------------------------

This file contains a description of how to set up the Blackfin toolchain using
the files contained in BF_Toolchain.zip.

Josh Fromm 2014
Last edit: Danielle Zhu, 13 May 2015

Setting Up:

Install the ADI toolchain. Included in the zip is an executable called
blackfin-toolchain-win32... Simply run this on a computer to install it.

Next you need to add inpout32.dll to C:\WINDOWS\system32 to install parallel
port compatibility

Finally, you need to copy the urjtag folder into \usr\local\share in whatever
drive you expect people to be debugging on (E or C probably). This allows
the blackfin debugging software to properly identify a CPU.

Building the Code:

To build an elf file for the system, simply use make bfinmp3. To make an ldr
file that can be loaded onto the ROM, use make bfinstart instead. When new files
are added to the system (audio.s for example), the makefile must be updated to
include it. The makefile is commented and indicates the proper way to do this.
Note that all student code is expected to go in src/sys while Glen code goes in
src/bfin101.

If you want to suppress the code output when using make, write the command:
make bfinmp3 > output.txt
This will put all the output code in a text file instead of outputting it into
the command window, so that you will be able to see all the error messages more
easily.

Running Code:

To run code, you should have students make a copy of BF_Toolchain that they'll
work in. They can then navigate to their folder and connect to a board by
first issuing the command

>> bfin-gdbproxy bfin --connect="cable wiggler parallel 0x378"

Next, another terminal must be opened and the folder must again be navigated to.
In this terminal, issue the command

>> bfin-elf-gdb

Once gdb starts you have to connect to gdbproxy using

gdb >> target remote :2000

If your processor is working properly, you should see gdbproxy report that
connection was successful. You can then load symbols by doing

gdb >> file bfinmp3.elf

And load the code onto the processor with

gdb >> load bfinmp3.elf

To run the code you have to use continue rather than run. From this point on,
gdb doesnt require any other special treatment (it runs as gdb would debugging
any code).

If you run into CreateProcess: No such file or directory error, do

