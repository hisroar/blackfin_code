##############################################################################
#                                                                            #
#                                 int.inc                                    #
#                            Interrupt Constants                             #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for interrupts.
#
# Revision History:
# 	04/28/16    Dennis Shim     initial revision
# 	06/01/16 	Dennis Shim 	added PF B constants and modified masking constants
#

# System Interrupt Mask Register (SIC_IMASK)
.equ SIC_IMASK_VAL, 0x00080000  # value of the SIC_IMASK register
                                # enables PF Interrupt A, PF Interrupt B initially
.equ DISABLE_SPI, 0xFFFFCFFF 	# value to AND with to disable SPI interrupt
.equ ENABLE_SPI, 0x00002000 	# value to OR with to enable SPI interrupt
.equ DISABLE_PFB, 0xFFEFFFFF 	# value to AND with to disable PF B interrupt
.equ ENABLE_PFB, 0x00100000 	# value to OR with to enable PF B interrupt

.equ SIC_IMASK_ADR, 0xFFC0010C  # address of the SIC_IMASK register

# Core Interrupt Mask Register (IMASK)
.equ IMASK_VAL, 0x0000345F      # value of the IMASK register
                                # enables core timer interrupts, IVG13, 12, 10

.equ IMASK_ADR, 0xFFE02104      # address of the IMASK register

# System Interrupt Assignment Registers (SIC_IARx)
.equ SIC_IAR1_VAL, 0x22322221 	# value of the SIC_IAR1 register
								# 0x2-------  UART TX  				- IVG9
								# 0x-2------  UART RX				- IVG9
								# 0x--3-----  SPI 					- IVG10
								# 0x---2----  SPORT1 TX				- IVG9
								# 0x----2---  SPORT1 RX				- IVG9
								# 0x-----2--  SPORT0 TX				- IVG9
								# 0x------2-  SPORT0 RX				- IVG9
								# 0x-------1  PPI					- IVG8
.equ SIC_IAR1_ADR, 0xFFC00114   # address of the SIC_IAR1 register

.equ SIC_IAR2_VAL, 0x77765444 	# value of the SIC_IAR2 register
								# 0x7-------  software watchdog 	- IVG14
								# 0x-7------  memory DMA stream 1   - IVG14
								# 0x--7-----  memory DMA stream 2   - IVG14
								# 0x---6----  PF B interrupt 		- IVG13
								# 0x----5---  PF A interrupt 		- IVG12
								# 0x-----4--  Timer 2 				- IVG11
								# 0x------4-  Timer 1 				- IVG11
								# 0x-------4  Timer 0 				- IVG11
.equ SIC_IAR2_ADR, 0xFFC00118   # address of the SIC_IAR2 register