##############################################################################
#                                                                            #
#                               keypad.inc                                   #
#                            Keypad Constants                                #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for the display.
#
# Revision History:
# 	04/28/16    Dennis Shim     initial revision
#

.equ DEBOUNCE_TIME, 0x0032          # number of ms to debounce for (32H = 50 ms)

.equ SELECT_KEY_PRESSED, 0x00000000 # select key is pressed if it is zero
.equ SHIFT_KEY_PRESSED, 0x01        # shift key value for when shift is pressed
.equ SHIFT_KEY_NOT_PRESSED, 0x00    # shift key value for when shfit is not pressed

.equ ENCODER_PF_CW, 0x00000800      # value of PF11 and PF12 if rotary encoder
                                    #   was rotated clockwise
.equ ENCODER_PF_CCW, 0x00001000     # value of PF11 and PF12 if rotary encoder
                                    #   was rotated counterclockwise

.equ SHIFT_KEY_INIT, 0xFFFF         # initial value of shift key
                                    
# key codes
.equ KEY_TRACKUP,     0 			# keycode for track up
.equ KEY_TRACKDOWN,   1 			# keycode for track down
.equ KEY_PLAY,        2 			# keycode for play
.equ KEY_RPTPLAY,     3 			# keycode for repeat play
.equ KEY_FASTFWD,     4 			# keycode for fast forward
.equ KEY_REVERSE,     5 			# keycode for reverse
.equ KEY_STOP,        6 			# keycode for stop
.equ KEY_ILLEGAL,     7 			# keycode for illegal key
.equ NUM_KEYS,        7 			# number of valid keys
.equ FIRST_KEY,       KEY_TRACKUP 	# first key before wrapping
.equ LAST_KEY,        KEY_STOP 		# last key before wrapping

.equ ENCODER_TIME_DELAY, 100        # number of ms to prevent rotary encoder from
                                    #   creating more interrupts