//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                mem.s                                     //
//                           Memory Functions                               //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for accessing memory. The functions included
    are:
        _dram_refresh               - Refreshes DRAM. (PUBLIC)
        _dram_clear                 - Zeroes the DRAM. (PUBLIC)
        _get_blocks                 - Transfers data from address in IDE to DRAM.
                                      (PUBLIC)
        _ide_init                   - Initializes the IDE registers. (PUBLIC)
        _init_chs_mode              - Initializes CHS mode.
        _ide_check_busy             - Blocks until IDE is not busy.
        _ide_check_cmd_ready        - Blocks until IDE is ready for command.
        _ide_check_data_ready       - Blocks until IDE is ready to send data.
        _ide_read_data              - Reads one word of data from the IDE.

    Revision History:
    05/14/16    Dennis Shim     initial revision
    06/14/16    Dennis Shim     updated comments
*/

.global _dram_refresh
.global _dram_clear
.global _get_blocks
.global _ide_init

/* Include constants from mem.inc */
.include "src/sys/mem.inc"
.include "src/sys/int.inc"

/*
_get_blocks

Description:        Transfers data from the specified address in the IDE to an
                    address in the DRAM.

Operation:          Compute sector by taking LBA mod SectorsPerTrack. Then get
                    head by taking (LBA / SectorsPerTrack) mod HeadsPerCyl. Then
                    get cylinder number with LBA / (SectorsPerTrack*HeadsPerCyl).
                    Write these in the correct registers for IDE. Send the load
                    blocks command to IDE. Read sec_num * IDE_BLOCK_SIZE number
                    of words from the data register and store them in DRAM.

Arguments:          LBA_adr  - R0 - LBA address to convert to CHS
                    sec_num  - R1 - number of sectors to transfer
                    dest_adr - R2 - address of destination
Return Value:       None.

Local Variables:    sec_num  - R6 - number of sectors to transfer
                    dest_adr - P1 - address of destination

Shared Variables:   SectorsPerTrack - number of sectors per track. [H]
                    HeadsPerCyl     - number of heads per cylinder. [H]
Global Variables:   None.

Input:              Reads data from IDE.
Output:             Sends commands and data to IDE registers.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_get_blocks:
    [--SP] = P1;                        // preserve registers
    [--SP] = P0;
    [--SP] = R6;
    [--SP] = R5;
    [--SP] = R4;
    [--SP] = R3;
    [--SP] = R2;
    [--SP] = R1;

    R6 = R1;                            // preserve arguments because divide
    P1 = R2;                            //   changes R0-3

ComputeSector:
    P0.L = SectorsPerTrack;             // prepare to modulo by SectorsPerTrack
    P0.H = SectorsPerTrack;
    R1.L = W[P0];
    R1.H = 0;                           // clear any junk from top halfword

    // take LBA mod SectorsPerTrack, R3 contains sector
    [--SP] = RETS;                      // preserve return address
    CALL _divide;
    RETS = [SP++];                      // restore return address

    R3 += 1;                            // increment by 1 to get sector number

    // prepare to write sector to sector number address
    // block until not busy 
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_SECT_NMB_ADR;            // write to the sector number register
    P0.H = IDE_SECT_NMB_ADR;
    B[P0] = R3;

ComputeHead:
    R0 = R2;                            // prepare to divide (LBA / SectorsPerTrack)
    P0.L = HeadsPerCyl;                 // prepare to modulo by HeadsPerCyl
    P0.H = HeadsPerCyl;
    R1.L = W[P0];                       // note that top halfword should be clear already

    // take (LBA/SectorsPerTrack) mod HeadsPerCyl, R3 contains head, R2 contains
    //   cylinder number
    [--SP] = RETS;                      // preserve return address
    CALL _divide;
    RETS = [SP++];                      // restore return address

    // prepare to write head to device/head register
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_DEVICE_ADR;              // prepare to read/write from register
    P0.H = IDE_DEVICE_ADR;
    R0 = B[P0];                         // get current value
    R1.L = LOWER_4BIT_MASK;             // mask out lower 4 bits to write new val
    R1.H = LOWER_4BIT_MASK;
    R0 = R0 & R1;

    R0 = R0 | R3;                       // assuming head bits above 4 are zero,
                                        //   we can just OR with R0 to get reg val

    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    B[P0] = R0;                         // write reg val to device/head register

    // block until cmd ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_cmd_ready;
    RETS = [SP++];                      // restore return address

ComputeCylinder:
    // we already have cylinder number in R2, just write it

    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_CYL_LOW_ADR;             // prepare to write to lower cylinder
    P0.H = IDE_CYL_LOW_ADR;
    B[P0] = R2;

    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    R2 >>= 8;                           // shift R2 by 8 so we can write high byte
    P0.L = IDE_CYL_HIGH_ADR;            // prepare to write to high cylinder
    P0.H = IDE_CYL_HIGH_ADR;
    B[P0] = R2;

WriteSectorsToTransfer:
    // write # sectors to transfer (R6)

    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_SECT_CNT_ADR;            // prepare to write to sector count register
    P0.H = IDE_SECT_CNT_ADR;

    B[P0] = R6;

LoadBlocks:
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    // block until cmd ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_cmd_ready;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_CMD_ADR;                 // prepare to send command to load blocks
    P0.H = IDE_CMD_ADR;
    R0.L = IDE_CMD_LOAD_BLOCKS;         // load correct command
    R0.H = IDE_CMD_LOAD_BLOCKS;
    B[P0] = R0;

GetDataBlocks:
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    /*
    R5.L = IDE_BLOCK_SIZE;              // block size of the IDE
    R5.H = IDE_BLOCK_SIZE;

    R4 = R5 * R6;                       // get the number of 2 byte words to read
    */
    R4 = R6;                            // get number of 2 byte words to read
    R4 <<= 8;                           // multiply by IDE_BLOCK_SIZE
    
    R1 = 0;                             // use R1 as a counter of how many words
                                        //   we've read

    P0.L = IDE_DATA_ADR;                // prepare to read from data register
    P0.H = IDE_DATA_ADR;
    
    // block until data ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_data_ready;
    RETS = [SP++];                      // restore return address

ReadDataCondition:
    CC = R1 < R4;                       // check if we've all the words
    //if CC   JUMP ReadDataBody;        // we haven't, read a word
    if !CC  JUMP GetBlocksEnd;          // we have, check how many errors

ReadDataBody:                           // read a block from memory and write it
    // read data from IDE
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    W[P1] = R0.L;                       // write that 16-bit word to DRAM

    P1 += 2;                            // increment DRAM address by 2
    R1 += 1;                            // increment the counter since we read a word

    JUMP ReadDataCondition;    

GetBlocksEnd:
    P0.L = IDE_SECT_CNT_ADR;            // check how many sectors in error
    P0.H = IDE_SECT_CNT_ADR;
    R0 = B[P0];

    R0 = R6 - R0;                       // compute number of sectors without error

    R1 = [SP++];                        // restore registers
    R2 = [SP++];
    R3 = [SP++];
    R4 = [SP++];
    R5 = [SP++];
    R6 = [SP++];
    P0 = [SP++];
    P1 = [SP++];
    
    RTS;
    
._get_blocks.END:

/*
_ide_init

Description:        Initializes the IDE and shared variables.

Operation:          Initialize the IDE in CHS mode. Get the HeadsPerCyl and
                    SectorsPerTrack and store them in the shared variables.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   SectorsPerTrack - number of sectors per track. [H]
                    HeadsPerCyl     - number of heads per cylinder. [H]
Global Variables:   None.

Input:              None.
Output:             Initialize IDE registers.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_ide_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;

InitCHS:
    // initialize the IDE in CHS mode
    [--SP] = RETS;                      // preserve return address
    CALL _init_chs_mode;
    RETS = [SP++];                      // restore return address

SendIDCmd:
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    // block until cmd ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_cmd_ready;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_CMD_ADR;                 // send a command to command register
    P0.H = IDE_CMD_ADR;
    R0.L = IDE_CMD_ID_DEVICE;           // send the ID device command
    R0.H = IDE_CMD_ID_DEVICE;
    B[P0] = R0;

    // block until data ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_data_ready;
    RETS = [SP++];                      // restore return address

ReadUselessData1:                       // when you're too lazy to do a for loop
    // read some useless data from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    // read some useless data from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    // read some useless data from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

ReadLogicalHeads:
    // read the number of logical heads from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    P0.L = HeadsPerCyl;                 // prepare to write to HeadsPerCyl
    P0.H = HeadsPerCyl;

    W[P0] = R0.L;                       // write # logical heads to HeadsPerCyl

ReadUselessData2:                       // when you're too lazy to do a for loop
    // read some useless data from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    // read some useless data from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

ReadLogicalSectors:
    // read the number of logical sectors per track from the buffer
    [--SP] = RETS;                      // preserve return address
    CALL _ide_read_data;
    RETS = [SP++];                      // restore return address

    P0.L = SectorsPerTrack;             // prepare to write to SectorsPerTrack
    P0.H = SectorsPerTrack;

    W[P0] = R0.L;                       // write # logical heads to SectorsPerTrack

IDEInitEnd:
    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._ide_init.END:

/*
_init_chs_mode

Description:        Initializes IDE in CHS mode.

Operation:          Check IDE busy. Read in the device/head register from IDE.
                    AND it with a mask to zero the 4th and 6th bits. Check busy
                    again. Write the new value to device/head register. Check
                    command ready.

Arguments:          None.
Return Value:       None.

Local Variables:    init_val - R0 - value to initialize device/head register to
Shared Variables:   None.
Global Variables:   None.

Input:              Reads device/head register from IDE.
Output:             Writes to device/head register in IDE.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_init_chs_mode:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    P0.L = IDE_DEVICE_ADR;              // read from the device/head register
    P0.H = IDE_DEVICE_ADR;
    R0 = B[P0];                         // only read a byte from the address

    R1.L = IDE_CHS_INIT;                // mask out the 4th and 6th bits
    R1.H = IDE_CHS_INIT;

    R0 = /*R0 & */R1;                   // DEBUG - wouldn't initialize correctly

    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address

    B[P0] = R0;                         // write the new register value to the
                                        //   device/head register

    // block until cmd ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_cmd_ready;
    RETS = [SP++];                      // restore return address

    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._init_chs_mode.END:

/*
_ide_check_busy

Description:        Blocks until the IDE is not busy.

Operation:          Loop while reading in the IDE register and checking if the
                    busy flag was set. If it is set, keep looping. Otherwise,
                    return.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              Reads status register from IDE.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_ide_check_busy:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    
    P0.L = IDE_STATUS_ADR;              // we're reading from the IDE status register
    P0.H = IDE_STATUS_ADR;
    R1.L = IDE_KEEP_BUSY_BIT;           // we only need to keep busy flag
    R1.H = IDE_KEEP_BUSY_BIT;

CheckBusy:
    R0 = B[P0];                         // read in the status register
    R0 = R0 & R1;                       // only keep the busy flag

    CC = R0 == 0;                       // check if busy flag is off
    if !CC  JUMP CheckBusy;             // busy flag is on, keep blocking
    //if CC   JUMP CheckBusyEnd;        // busy flag is off, stop blocking

CheckBusyEnd:
    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._ide_check_busy.END:

/*
_ide_check_cmd_ready

Description:        Blocks until the IDE is ready to accept commands.

Operation:          Loop while reading in the IDE register and checking if the
                    cmd ready was set. If it is not set, keep looping. Otherwise,
                    return.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              Reads status register from IDE.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_ide_check_cmd_ready:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    
    P0.L = IDE_STATUS_ADR;              // we're reading from the IDE status register
    P0.H = IDE_STATUS_ADR;
    R1.L = IDE_KEEP_CMD_BIT;            // we only need to keep cmd rdy flag
    R1.H = IDE_KEEP_CMD_BIT;

CheckCmdRdy:
    R0 = B[P0];                         // read in the status register
    R0 = R0 & R1;                       // only keep the cmd rdy flag

    CC = R0 == 0;                       // check if cmd rdy flag is off
    if CC  JUMP CheckCmdRdy;            // cmd rdy flag is off, keep blocking
    //if !CC   JUMP CheckCmdRdyEnd;     // cmd rdy flag is on, stop blocking

CheckCmdRdyEnd:
    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._ide_check_cmd_ready.END:

/*
_ide_check_data_ready

Description:        Blocks until the IDE is ready to send data.

Operation:          Loop while reading in the IDE register and checking if the
                    data ready was set. If it is not set, keep looping. Otherwise,
                    return.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              Reads status register from IDE.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_ide_check_data_ready:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    
    P0.L = IDE_STATUS_ADR;              // we're reading from the IDE status register
    P0.H = IDE_STATUS_ADR;
    R1.L = IDE_KEEP_DATA_BIT;           // we only need to keep data rdy flag
    R1.H = IDE_KEEP_DATA_BIT;

CheckDataRdy:
    R0 = B[P0];                         // read in the status register
    R0 = R0 & R1;                       // only keep the data rdy flag

    CC = R0 == 0;                       // check if data rdy flag is off
    if CC  JUMP CheckDataRdy;           // cmd rdy flag is off, keep blocking
    //if !CC   JUMP CheckDataRdyEnd;    // cmd rdy flag is on, stop blocking

CheckDataRdyEnd:
    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._ide_check_data_ready.END:

/*
_ide_read_data

Description:        Reads a word from the IDE data register.

Operation:          Checks busy. Disables interrupts. Checks data ready. Reads a
                    word from the data register. Returns it in R0.L.

Arguments:          None.
Return Value:       data - R0 - word of data read from the IDE.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              Reads data register from IDE.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/21/16     Dennis Shim      initial revision
*/
_ide_read_data:
    [--SP] = P0;                        // preserve registers
    [--SP] = P1;
    [--SP] = R1;
    
    // block until not busy
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_busy;
    RETS = [SP++];                      // restore return address
    
    // disable interrupts
    P1.L = IMASK_ADR;                   // get IMASK register address
    P1.H = IMASK_ADR;
    R1 = [P1];
    R0 = 0;
    [P1] = R0;
    
    // block until data ready
    [--SP] = RETS;                      // preserve return address
    CALL _ide_check_data_ready;
    RETS = [SP++];                      // restore return address
    
    P0.L = IDE_DATA_ADR;                // read from the IDE data register
    P0.H = IDE_DATA_ADR;
    R0.L = W[P0];                       // read from the data register
    R0.H = 0;                           // clear junk from R0.H
    
    //enable interrupts
    [P1] = R1; 

    R1 = [SP++];                        // restore registers
    P1 = [SP++];
    P0 = [SP++];
    
    RTS;
    
._ide_read_data.END:

/*
_dram_refresh

Description:        Refreshes the DRAM.

Operation:          Read from the DRAM without setting A19 to refresh.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Refreshes DRAM.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/14/16    Dennis Shim      initial revision
*/
_dram_refresh:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Read from the DRAM
    P0.L = DRAM_ADR;                    // get TCNTL register address
    P0.H = DRAM_ADR;
    
    R0 = [P0];                          // read from the DRAM with A19 not set
                                        //   to refresh it
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._dram_refresh.END:

/*
_dram_clear

Description:        Clears the DRAM.

Operation:          Writes zeroes to all DRAM addresses

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Clears DRAM.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/14/16    Dennis Shim      initial revision
*/
_dram_clear:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    R0 = 0;
    R1.L = DRAM_SIZE;
    R1.H = DRAM_SIZE;
    P0.L = DRAM_ADR;
    P0.H = DRAM_ADR;
    R2 = 0;

ClearLoopCondition:
    CC = R2 < R1;
    if !CC JUMP ClearEnd;

ClearLoopBody:
    B[P0] = R0;
    P0 += 1;
    R2 += 1;
    JUMP ClearLoopCondition;

ClearEnd:
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._dram_clear.END:

// Data segment
.section .data
SectorsPerTrack:                    // number of sectors per track
    .space 2
HeadsPerCyl:                        // number of heads per cylinder
    .space 2

.end
