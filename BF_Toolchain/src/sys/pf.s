//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                                 pf.s                                     //
//                             PF Functions                                 //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for initializing and accessing PFs. The functions
    included are:
        _PF_init                - Initializes PFs (interrupts, I/O). (PUBLIC)
        _PF_A_event_handler     - Event handler for the PF A interrupts. (PUBLIC)
        _install_PF_A_EH        - Installs event handler for the PF A interrupt (PUBLIC)
        _get_PF                 - Returns the PF values in R0.L (PUBLIC)
        _data_PF                - Sets all PF values based on R0 input (PUBLIC)
        _set_PF                 - Sets PF values based on R0 input (PUBLIC)
        _clear_PF               - Clears PF values based on R0 input (PUBLIC)
    Revision History:
    04/28/16    Dennis Shim     initial revision
    06/14/16    Dennis Shim     updated comments
*/

.global _PF_init
.global _PF_A_event_handler
.global _PF_B_event_handler
.global _install_PF_A_EH
.global _install_PF_B_EH
.global _get_PF
.global _data_PF
.global _set_PF
.global _clear_PF

/* Include constants from pf.inc */
.include "src/sys/pf.inc"

/*
_PF_init

Description:        Initialize the Blackfin PFs.

Operation:          Set the direction of PF registers using the Flag Direction
                    register. Set which PFs trigger interrupt A using Flag Mask
                    Interrupt A Set Register. Set PF edge sensitivity using the
                    Flag Interrupt Sensitivity Register. Using the Flag Set On
                    Both Register make both high and low edges trigger interrupts.
                    Enable input buffers using Flag Input Enable register.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             FIO_DIR, FIO_MASKA_D, FIO_EDGE, FIO_BOTH, and FIO_INEN
                    registers are changed.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_PF_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Initialize FIO_DIR register - make all PFs inputs
    P0.L = FIO_DIR_ADR;                 // get FIO_DIR register address
    P0.H = FIO_DIR_ADR;
    R0.L = FIO_DIR_VAL;                 // get FIO_DIR value
    
    W[P0] = R0.L;                       // write FIO_DIR value to the address
    
    // Initialize FIO_MASKA_D register - make PF12, PF11 trigger interrupt A
    P0.L = FIO_MASKA_D_ADR;             // get FIO_MASKA_D register address
    P0.H = FIO_MASKA_D_ADR;
    R0.L = FIO_MASKA_D_VAL;             // get FIO_MASKA_D value
    
    W[P0] = R0.L;                       // write FIO_MASKA_D value to the address

    // Initialize FIO_MASKB_D register - make PF15 trigger interrupt B
    P0.L = FIO_MASKB_D_ADR;             // get FIO_MASKB_D register address
    P0.H = FIO_MASKB_D_ADR;
    R0.L = FIO_MASKB_D_VAL;             // get FIO_MASKB_D value
    
    W[P0] = R0.L;                       // write FIO_MASKB_D value to the address

    // Initialize FIO_EDGE register - make PF12, PF11 edge sensitive
    P0.L = FIO_EDGE_ADR;                // get FIO_EDGE register address
    P0.H = FIO_EDGE_ADR;
    R0.L = FIO_EDGE_VAL;                // get FIO_EDGE value
    
    W[P0] = R0.L;                       // write FIO_EDGE value to the address

    // Initialize FIO_POLAR register - make PF12, PF11 trigger interrupts on low edges
    P0.L = FIO_POLAR_ADR;               // get FIO_POLAR register address
    P0.H = FIO_POLAR_ADR;
    R0.L = FIO_POLAR_VAL;               // get FIO_POLAR value
    
    W[P0] = R0.L;                       // write FIO_POLAR value to the address

    // Initialize FIO_INEN register - buffer PF14-11 inputs
    P0.L = FIO_INEN_ADR;                // get FIO_INEN register address
    P0.H = FIO_INEN_ADR;
    R0.L = FIO_INEN_VAL;                // get FIO_INEN value
    
    W[P0] = R0.L;                       // write FIO_INEN value to the address
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._PF_init.END:

/*
_PF_A_event_handler

Description:        PF interrupt A event handler. Changes value of current key.

Operation:          Preserve all registers. Calls rotary encoder functions.
                    Restore all registers.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_PF_A_event_handler:
    [--SP] = ASTAT;                     // preserve registers
    [--SP] = RETI;
    [--SP] = R0;
    
    // change the value of the current_key based on PFs
    [--SP] = RETS;					    // preserve return address
    CALL _key_current_value_change;
    RETS = [SP++];                      // restore return address
    
    R0.L = PF_A_INT_CLEAR;              // clear the interrupt
    [--SP] = RETS;					    // preserve return address
    CALL _clear_PF;
    RETS = [SP++];                      // restore return address
    
    R0 = [SP++];                        // restore registers
    RETI = [SP++];                      
    ASTAT = [SP++];
    
    RTI;
    
._PF_A_event_handler.END:

/*
_PF_B_event_handler

Description:        PF interrupt B event handler. Call demand_handler

Operation:          Preserve all registers. Calls demand handler. Restore all
                    registers.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16   Dennis Shim      initial revision
*/
_PF_B_event_handler:
    [--SP] = ASTAT;                     // preserve registers
    [--SP] = RETI;
    [--SP] = R0;
    
    // handle demand interrupt from SPI
    [--SP] = RETS;                      // preserve return address
    CALL _demand_handler;
    RETS = [SP++];                      // restore return address
    
    R0.L = PF_B_INT_CLEAR;              // clear the interrupt
    [--SP] = RETS;                      // preserve return address
    CALL _clear_PF;
    RETS = [SP++];                      // restore return address
    
    R0 = [SP++];                        // restore registers
    RETI = [SP++];                      
    ASTAT = [SP++];
    
    RTI;
    
._PF_B_event_handler.END:

/*
_install_PF_A_EH

Description:        Installs the PF A interrupt event handler in the event
                    vector table.

Operation:          Installs the PF A event handler in the event vector table.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_install_PF_A_EH:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    P0.L = EVT12_ADR;                   // get the address of EVT13
    P0.H = EVT12_ADR;
    R0.L = _PF_A_event_handler;         // put the PF A event handler in EVT 13
    R0.H = _PF_A_event_handler;

    [P0] = R0;
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._install_PF_A_EH.END:

/*
_install_PF_B_EH

Description:        Installs the PF B interrupt event handler in the event
                    vector table.

Operation:          Installs the PF B event handler in the event vector table.

Arguments:          None.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
                    value_temp - R0 - temporarily stores a register value
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    05/28/16   Dennis Shim      initial revision
*/
_install_PF_B_EH:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    
    P0.L = EVT13_ADR;                   // get the address of EVT13
    P0.H = EVT13_ADR;
    R0.L = _PF_B_event_handler;         // put the PF B event handler in EVT 13
    R0.H = _PF_B_event_handler;

    [P0] = R0;
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];
    
    RTS;
    
._install_PF_B_EH.END:

/*
_get_PF

Description:        Returns PF15:0 in R0.

Operation:          Read in the PF values from the Flag Data Register. 

Arguments:          None.
Return Value:       PF_values - R0.L - PF15:0 values

Local Variables:    addr_temp - P0 - temporarily stores a register address
                    PF_values - R0 - stores the PF values
Shared Variables:   None.
Global Variables:   None.

Input:              Reads from the Flag Data Register.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  R0.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_get_PF:
    [--SP] = P0;                    // preserve registers
    
    P0.L = FIO_FLAG_D_ADR           // prepare to read from the flag data register
    P0.H = FIO_FLAG_D_ADR
    R0.L = W[P0]                    // read from the flag data register
                                    // R0 now contains PF15:0
    
    P0 = [SP++];                    // restore registers
    
    RTS;
    
._get_PF.END:

/*
_data_PF

Description:        Sets PF15:0 to exactly R0.

Operation:          Set the PF values in the Flag Data Register using R0.L.

Arguments:          PF_values_set - R0.L - PF15:0 values to set exactly.
Return Value:       None.

Local Variables:    addr_temp - P0 - temporarily stores a register address
Shared Variables:   None.
Global Variables:   None.

Input:              Writes to the Flag Data Register.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  R0.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_data_PF:
    [--SP] = P0;                    // preserve registers
    
    P0.L = FIO_FLAG_D_ADR           // prepare to read from the flag data register
    P0.H = FIO_FLAG_D_ADR
    W[P0] = R0.L                    // read from the flag data register
                                    // [P0] now contains R0.L
    
    P0 = [SP++];                    // restore registers
    
    RTS;
    
._data_PF.END:

/*
_set_PF

Description:        Sets PF15:0 given their desired values in R0.

Operation:          Sets the PF values based on the given argument in R0 using
                    the FIO_FLAG_S register. 

Arguments:          PF_values_set - R0.L - PF15:0 values to set.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Outputs to the FIO_FLAG_S register.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  R0.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_set_PF:
    [--SP] = P0;                    // preserve registers
    
    P0.L = FIO_FLAG_S_ADR           // prepare to write to the flag set register
    P0.H = FIO_FLAG_S_ADR
    W[P0] = R0.L                    // write to the flag set register
    
    P0 = [SP++];                    // restore registers
    
    RTS;
    
._set_PF.END:

/*
_clear_PF

Description:        Clears certain PFs given their desired values in R0.

Operation:          Clears the PF values based on the given argument in R0 using
                    the FIO_FLAG_C register. 

Arguments:          PF_values_clear - R0.L - PF15:0 values to clear.
Return Value:       None.

Local Variables:    addr_temp  - P0 - temporarily stores a register address
Shared Variables:   None.
Global Variables:   None.

Input:              None.
Output:             Outputs to the FIO_FLAG_S register.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  R0.

Revision History:
    4/28/16   Dennis Shim      initial revision
*/
_clear_PF:
    [--SP] = P0;                    // preserve registers
    
    P0.L = FIO_FLAG_C_ADR           // prepare to write to the flag clear register
    P0.H = FIO_FLAG_C_ADR
    W[P0] = R0.L                    // write to the flag clear register to clear
    
    P0 = [SP++];                    // restore registers
    
    RTS;
    
._clear_PF.END:

.data

.end
