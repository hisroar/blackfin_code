##############################################################################
#                                                                            #
#                                  pf.inc                                    #
#                               PF Constants                                 #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for the PFs.
#
# Revision History:
# 	04/28/16    Dennis Shim     initial revision
#

# Flag Direction Register (FIO_DIR)
.equ FIO_DIR_VAL, 0x07FF  			# value of the FIO_DIR register
                                	# PF15:11 inputs, PF10:0 outputs
.equ FIO_DIR_INP, 0x0700            # PF 7:0 inputs to read busy flag

.equ FIO_DIR_ADR, 0xFFC00730  		# address of the FIO_DIR register

# Flag Mask Interrupt A Set Register (FIO_MASKA_D)
.equ FIO_MASKA_D_VAL, 0x1800  		# value of the FIO_MASKA_D register
                                	# PF12, PF11 have interrupt mask set

.equ FIO_MASKA_D_ADR, 0xFFC00710  	# address of the FIO_MASKA_D register

# Flag Mask Interrupt B Set Register (FIO_MASKB_D)
.equ FIO_MASKB_D_VAL, 0x8000  		# value of the FIO_MASKB_D register
                                	# PF15 have interrupt mask set

.equ FIO_MASKB_D_ADR, 0xFFC00720  	# address of the FIO_MASKB_D register

# Flag Interrupt Sensitivity Register (FIO_EDGE)
.equ FIO_EDGE_VAL, 0x1800  			# value of the FIO_EDGE register
                                	# PF12, PF11 are edge sensitive

.equ FIO_EDGE_ADR, 0xFFC00738  		# address of the FIO_EDGE register

# Flag Polarity Register (FIO_POLAR)
.equ FIO_POLAR_VAL, 0x1800          # value of the FIO_POLAR register
                                	# PF12, PF11 low edges trigger interrupts

.equ FIO_POLAR_ADR, 0xFFC00734      # address of the FIO_POLAR register

# Flag Input Enable Register (FIO_INEN)
.equ FIO_INEN_VAL, 0xF800  			# value of the FIO_INEN register
                                	# PF14, PF13, PF12, PF11 have input buffers

.equ FIO_INEN_ADR, 0xFFC00740  		# address of the FIO_INEN register

# Flag Data Register (FIO_FLAG_D)
.equ FIO_FLAG_D_ADR, 0xFFC00700		# address of the FIO_FLAG_D register

# Flag Set Register (FIO_FLAG_S)
.equ FIO_FLAG_S_ADR, 0xFFC00708		# address of the FIO_FLAG_S register

# Flag Clear Register (FIO_FLAG_C)
.equ FIO_FLAG_C_ADR, 0xFFC00704		# address of the FIO_FLAG_C register

# Event Vector Table - IVG12/13
.equ EVT12_ADR, 0xFFE02030 			# address of EVT12 (for PF interrupt A)
.equ EVT13_ADR, 0xFFE02034  		# address of EVT13 (for PF interrupt B)

# Miscellaneous Constants
.equ PF_A_INT_CLEAR, 0x1800         # value to pass to _clear_PF to clear PF A interrupt
.equ PF_B_INT_CLEAR, 0x8000 		# value to pass to _clear_PF to clear PF B interrupt

.equ KEEP_PF0_MASK,  0x00000001     # AND PF values with this value to keep only PF0
.equ KEEP_PF1_MASK,  0x00000002     # AND PF values with this value to keep only PF1
.equ KEEP_PF2_MASK,  0x00000004     # AND PF values with this value to keep only PF2
.equ KEEP_PF3_MASK,  0x00000008     # AND PF values with this value to keep only PF3
.equ KEEP_PF4_MASK,  0x00000010     # AND PF values with this value to keep only PF4
.equ KEEP_PF5_MASK,  0x00000020     # AND PF values with this value to keep only PF5
.equ KEEP_PF6_MASK,  0x00000040     # AND PF values with this value to keep only PF6
.equ KEEP_PF7_MASK,  0x00000080     # AND PF values with this value to keep only PF7
.equ KEEP_PF8_MASK,  0x00000100     # AND PF values with this value to keep only PF8
.equ KEEP_PF9_MASK,  0x00000200     # AND PF values with this value to keep only PF9
.equ KEEP_PF10_MASK, 0x00000400     # AND PF values with this value to keep only PF10
.equ KEEP_PF11_MASK, 0x00000800     # AND PF values with this value to keep only PF11
.equ KEEP_PF12_MASK, 0x00001000     # AND PF values with this value to keep only PF12
.equ KEEP_PF13_MASK, 0x00002000     # AND PF values with this value to keep only PF13
.equ KEEP_PF14_MASK, 0x00004000     # AND PF values with this value to keep only PF14
.equ KEEP_PF15_MASK, 0x00008000     # AND PF values with this value to keep only PF15
