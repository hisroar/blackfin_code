##############################################################################
#                                                                            #
#                                 mem.inc                                    #
#                             Memory Constants                               #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for the display.
#
# Revision History:
# 	05/14/16    Dennis Shim     initial revision
#

# DRAM values
.equ SET_A19, 0x00080000        	# OR with this value to set the 19th address bit
.equ DRAM_ADR, 0x20200000       	# first address of DRAM
.equ DRAM_SIZE, 0x10000             # size of the DRAM

# SRAM values
.equ SRAM_ADR, 0x20100000       	# first address of SRAM

# ROM values
.equ ROM_ADR, 0x20000000        	# first address of ROM

# IDE values
.equ IDE_ADR, 0x20300000        	# first address of IDE
.equ IDE_DATA_ADR, 0x20300000 		# address of the DATA register
.equ IDE_SECT_CNT_ADR, 0x20300004 	# address of the sector count register
.equ IDE_SECT_NMB_ADR, 0x20300006	# address of the sector number register
.equ IDE_CYL_LOW_ADR, 0x20300008	# address of the cylinder low register
.equ IDE_CYL_HIGH_ADR, 0x2030000A 	# address of the cylinder high register
.equ IDE_DEVICE_ADR, 0x2030000C		# address of the device/head register
.equ IDE_STATUS_ADR, 0x2030000E 	# address of the status register
.equ IDE_CMD_ADR, 0x2030000E 		# address of the command register

.equ IDE_KEEP_BUSY_BIT, 0x80 		# mask to AND with status byte to get busy
.equ IDE_KEEP_CMD_BIT, 0x40 		# mask to AND with status byte to get cmd ready 
.equ IDE_KEEP_DATA_BIT, 0x08 		# mask to AND with status byte to get data ready

.equ IDE_CHS_INIT, 0xAF 			# AND with this value to reset bits 4 and 6 to
									#   initialize CHS mode

.equ IDE_CMD_ID_DEVICE, 0xEC 		# command to ID device for the IDE
.equ IDE_CMD_LOAD_BLOCKS, 0x21 		# command to begin loading blcocks

.equ IDE_BLOCK_SIZE, 256 			# block size of the IDE

.equ LOWER_4BIT_MASK, 0xFFFFFFF0 	# masks out lower 4 bits
