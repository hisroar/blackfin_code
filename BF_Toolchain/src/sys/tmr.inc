##############################################################################
#                                                                            #
#                                 tmr.inc                                    #
#                            Timer Constants                                 #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for the timers.
#
# Revision History:
#       04/28/16    Dennis Shim     initial revision
#

# Core Timer Control Register (TCNTL)
.equ TCNTL_VAL, 0x00000007      # value of the TCNTL register
                                # xxxx xxxx xxxx xxxx xxxx xxxx xxxx ----  reserved
                                # ---- ---- ---- ---- ---- ---- ---- x---  TINT (timer generated interrupt?)
                                # ---- ---- ---- ---- ---- ---- ---- -1--  auto-reload from TPERIOD on
                                # ---- ---- ---- ---- ---- ---- ---- --1-  when TMPWR = 1, enable timer
                                # 0--- ---- ---- ---- ---- ---- ---- ---1  TMPWR, active state

.equ TCNTL_ADR, 0xFFE03000      # address of the TCNTL register

# Core Timer Period Register (TPERIOD)
.equ TPERIOD_VAL, 0x00030D40    # value of the TPERIOD register
                                # want interrupts to be at 1 kHz (T = 1 ms),
                                #   and we have CCLK = 200 MHz. Thus we get
                                #   TPERIOD_VAL = (200 MHz) / (1 kHz) = 200,000

.equ TPERIOD_ADR, 0xFFE03004    # address of the TPERIOD register

# Core Timer Scale Register (TSCALE)
.equ TSCALE_VAL, 0x00000000     # value of the TSCALE register
                                # want counter to go down every clock to get
                                #    correct interrupt frequency

.equ TSCALE_ADR, 0xFFE03008     # address of the TSCALE register

# Event Vector Table - IVTMR/EVT6
.equ EVT6_ADR, 0xFFE02018       # address of EVT6 (for core timer interrupt)
