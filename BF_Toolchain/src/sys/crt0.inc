##############################################################################
#                                                                            #
#                                crt0.inc                                    #
#                         Initialization Constants                           #
#                       Blackfin MP3 Jukebox Project                         #
#                                EE/CS 52                                    #
#                                                                            #
##############################################################################
#
# This file contains constants necessary for initializing the MP3 player.
#
# Revision History:
#     04/21/16    Dennis Shim     initial revision
#

.equ PLL_CTL_VAL, 0x0400            # value of the PLL_CTL register
                                    # ---- ---- ---0 -0--  reserved
                                    # 0--- ---- ---- ----  SPORT_HYS (no added hysteresis)
                                    # -000 010- ---- ----  MSEL[5:0] (mult factor for CLKIN/VCO)
                                    # ---- ---0 ---- ----  BYPASS (do not bypass PLL)
                                    # ---- ---- 0--- ----  OUT_DELAY (do not add output delay)
                                    # ---- ---- -0-- ----  IN_DELAY (do not add input delay)
                                    # ---- ---- --0- ----  PDWN (all internal clocks on)
                                    # ---- ---- ---- 0---  STOPCK (CCLK on)
                                    # ---- ---- ---- --0-  PLL_OFF (enable power to PLL)
                                    # ---- ---- ---- ---0  DF (Pass CLKIN to PLL)
.equ PLL_CTL_ADR, 0xFFC00000        # address of the PLL_CTL register

.equ PLL_DIV_VAL, 0x0001            # value of the PLL_DIV register
                                    # 0000 0000 00-- ----  reserved
                                    # ---- ---- --00 ----  core select (CCLK = VCO/1)
                                    # ---- ---- ---- 0001  system select (SCLK = VCO/1)
.equ PLL_DIV_ADR, 0xFFC00004        # address of the PLL_DIV register

.equ EBIU_AMGCTL_VAL, 0x00FE        # value of the EBIU_AMGCTL register
                                    # 0000 000- 1111 ----  reserved
                                    # ---- ---0 ---- ----  CDPRIO (core has prio over DMA)
                                    # ---- ---- ---- 1xx-  AMBEN[2:0] (All Banks enabled)
                                    # ---- ---- ---- ---0  AMCKEN (disable CLKOUT for async mem access)
.equ EBIU_AMGCTL_ADR, 0xFFC00A00    # address of the EBIU_AMGCTL register

.equ EBIU_AMBCTL0_VAL, 0xFFFEFFFE   # maximum timings for buses
.equ EBIU_AMBCTL0_ADR, 0xFFC00A04   # address of the EBIU_AMBCTL0 register

.equ EBIU_AMBCTL1_VAL, 0xFFFEFFFE   # maximum timing for buses
.equ EBIU_AMBCTL1_ADR, 0xFFC00A08   # address of the EBIU_AMBCTL1 register