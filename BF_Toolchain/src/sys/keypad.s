//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//                               keypad.s                                   //
//                           Keypad Functions                               //
//                      Blackfin MP3 Jukebox Project                        //
//                               EE/CS 52                                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/*
    This file contains functions for initializing and debouncing keys. The
    functions included are:
        _key_available              - Returns 1 in R0 if the select key was pressed
                                      and debounced. (PUBLIC)
        _getkey                     - Returns the key_code, which contains the
                                      value of the key that was pressed. (PUBLIC)
        _keypad_init                - Initializes keypad variables. (PUBLIC)
        _key_debounce_select        - Debounce the select key. (PUBLIC)
        _key_debounce_shift         - Debounce the shift key. (PUBLIC) (UNUSED)
        _key_current_value_change   - Change the value of the current_key based
                                      on which way the rotary encoder was rotated.
                                      (PUBLIC)
        _encoder_time_debounce      - Debounces the encoder_time_delay (PUBLIC)
    Revision History:
    04/28/16    Dennis Shim     initial revision
    06/14/16    Dennis Shim     updated comments
*/

.global _key_available
.global _getkey
.global _keypad_init
.global _key_debounce_select
.global _key_debounce_shift
.global _key_current_value_change
.global _encoder_time_debounce

// EXTERNS
.extern _get_PF     // get the PF values
.extern _display_encoder // display the encoder status  

/* Include constants from keypad.inc, pf.inc, and general.inc */
.include "src/sys/keypad.inc"
.include "src/sys/pf.inc"
.include "src/sys/general.inc"

/*
_key_available

Description:        Returns TRUE in R0 if the select key was debounced, FALSE otherwise.

Operation:          Put select_key_flag in R0 and return it.

Arguments:          None.
Return Value:       key_pressed - R0 - TRUE if the select key was debounced, FALSE otherwise

Local Variables:    None.
Shared Variables:   select_key_flag - TRUE if select key is available, FALSE otherwise [B]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_key_available:
    [--SP] = P0;                        // preserve registers
    
    P0.L = select_key_flag;             // prepare to move select_key_flag
    P0.H = select_key_flag;
    R0 = B[P0];                         // return the select_key_flag into R0
    
    P0 = [SP++];                        // restore registers

	RTS;

._key_available.END:

/*
_getkey

Description:        Returns the value of the keycode in R0.

Operation:          Put key_code in R0 and return it. Reset key_flag to FALSE.

Arguments:          None.
Return Value:       key_code - R0 - value of the key that was selected

Local Variables:    None.
Shared Variables:   key_code        - value of the key to return in getKey [B]
                    select_key_flag - TRUE if select key is available, FALSE otherwise [B]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_getkey:
    [--SP] = P0;                        // preserve registers
    
    P0.L = select_key_flag;             // prepare to reset select_key_flag
    P0.H = select_key_flag;
    R0 = FALSE;                         // reset select_key_flag to FALSE
    B[P0] = R0;                         // select_key_flag = FALSE

    P0.L = key_code;                    // return key_code in R0
    P0.H = key_code;
    R0 = B[P0];                         // R0 = key_code
    /*
    P0.L = current_key;                 // return current_key in R0
    P0.H = current_key;
    R0 = B[P0];                         // R0 = current_key
    */
    P0 = [SP++];                        // restore registers

	RTS;

._getkey.END:

/*
_keypad_init

Description:        Initialize the keypad variables to prepare for debouncing
                    and reading in rotary encoder input.

Operation:          Sets the debounce counters (debounce_counter_shift and 
                    debounce_counter_select) to DEBOUNCE_TIME. Set the select_key_flag
                    to zero because the select key is not pressed initially. Set
                    shift_key_value to SHIFT_KEY_NOT_PRESSED initially. Set the
                    current_key to FIRST_KEY. Initialize shift_key_prev to
                    SHIFT_KEY_INIT. Initialize encoder_time_delay to
                    ENCODER_INT_TIME_DELAY to prevent noise from affect the
                    system.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   debounce_counter_shift  - debounce counter for shift key [H]
                    debounce_counter_select - debounce counter for select key [H]
                    select_key_flag - TRUE if select key is available, FALSE otherwise [B]
                    shift_key_value - SHIFT_KEY_PRESSED if shift key is pressed,
                                      SHIFT_KEY_NOT_PRESSED otherwise [B]
                    current_key     - value of the key currently selected [B]
                    key_code        - value of the key to return in getKey [B]
                    shift_key_prev  - contains the previous shift key value [H]
                    encoder_time_delay - time delay for the rotary encoder before
                                         it can cause interrupts after it has
                                         been rotated. [H]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_keypad_init:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;

    // Initialize debouncing variables to DEBOUNCE_TIME
    R0.L = DEBOUNCE_TIME;               // value to put into the counters
    
    P0.L = debounce_counter_shift;      // first do shift debounce counter
    P0.H = debounce_counter_shift;
    W[P0] = R0.L;                       // debounce_counter_shift = DEBOUNCE_TIME
    
    P0.L = debounce_counter_select;     // then do select debounce counter
    P0.H = debounce_counter_select;
    W[P0] = R0.L;                       // debounce_counter_select = DEBOUNCE_TIME
    
    // select key flag should be initialized so there is initially no key available
    R0 = FALSE;                         // value to put in select_key_flag
    P0.L = select_key_flag;             // select flag address
    P0.H = select_key_flag;
    B[P0] = R0;                         // select_key_flag = FALSE
    
    // shift key value should be initialized so it is initially not pressed
    R0 = SHIFT_KEY_NOT_PRESSED;         // initially the shift key is not pressed
    P0.L = shift_key_value;             // shift flag address
    P0.H = shift_key_value;
    B[P0] = R0;                         // shift_key_value = SHIFT_KEY_NOT_PRESSED
    
    // Current key and key_code should be initialized to KEY_TRACKUP
    P0.L = current_key;                 // gotta get the address first
    P0.H = current_key;
    R0 = FIRST_KEY;
    B[P0] = R0;                         // current_key = FIRST_KEY
    
    P0.L = key_code;                    // gotta get the address first
    P0.H = key_code;
    R0 = FIRST_KEY;
    B[P0] = R0;                         // key_code = FIRST_KEY
    
    // Initialize shift_key_prev
    P0.L = shift_key_prev;              // get the address first
    P0.H = shift_key_prev;
    R0.L = SHIFT_KEY_INIT;
    W[P0] = R0.L                        // shift_key_prev = SHIFT_KEY_INIT
    
    // Initialize encoder_time_delay
    P0.L = encoder_time_delay;          // get the address first
    P0.H = encoder_time_delay;
    R0.L = ENCODER_TIME_DELAY;
    W[P0] = R0.L                        // encoder_time_delay = ENCODER_TIME_DELAY
    
    R0 = [SP++];                        // restore registers
    P0 = [SP++];

	RTS;

._keypad_init.END:

/*
_key_debounce_select

Description:        Debounces the select key.

Operation:          Check if the select key is pressed (PF13 should be low if it
                    is) by ANDing the PF values with the KEEP_PF13_MASK. If the
                    key is pressed (low), then decrement debounce_counter_select
                    if it is not zero. If the key is not pressed, set
                    debounce_counter_select to DEBOUNCE_TIME. If
                    debounce_counter_select is zero, then set the select_key_flag
                    and copy the current_key to key_code.

Arguments:          None.
Return Value:       None.

Local Variables:    debounce_counter_temp - R0.L - contains a temp copy of counter
Shared Variables:   debounce_counter_select - debounce counter for select key [H]
                    select_key_flag - TRUE if select key is available, FALSE otherwise [B]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_key_debounce_select:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;

    // Get the PF values in R0.L
    [--SP] = RETS;					    // preserve return address
    CALL _get_PF;
    RETS = [SP++];                      // restore return address
    
    R1 = KEEP_PF13_MASK;
    R0 = R0 & R1;                       // keep only PF13
    
    CC = R0 == KEY_PRESSED;             // check if the key is pressed
    if CC   JUMP SelectKeyPressed;      // key was pressed, debounce
    //if !CC  JUMP SelectKeyNotPressed; // key was not pressed, set counter to DEBOUNCE_TIME
    
SelectKeyNotPressed:                    // key wasn't pressed, counter = DEBOUNCE_TIME
    P0.L = debounce_counter_select;     // write to the select debounce counter
    P0.H = debounce_counter_select;
    R0.L = DEBOUNCE_TIME;               // prepare to write DEBOUNCE_TIME
    W[P0] = R0.L;                       // debounce_counter_select = DEBOUNCE_TIME
    
    JUMP SelectDebounceEnd;
    
SelectKeyPressed:                       // key was pressed, decrement counter if
                                        //   it is non-zero
    P0.L = debounce_counter_select;     // read the select debounce counter
    P0.H = debounce_counter_select;
    R0.L = W[P0];                       // assign the counter temporarily to R0.L
    R0.H = 0;                           // clear the high bit so we can compare
                                        //   the whole thing to zero
    CC = R0 == 0;                       // check if debounce counter is zero
    if CC   JUMP SelectDebounceEnd;     // it is, we've already debounced, just
                                        //   jump to the end because we're done
    //if !CC  JUMP DecrementSelectCounter;// it isn't, decrement it and check if
                                        //   it's zero
    
DecrementSelectCounter:                 // decrement the select debounce counter
    R1 = 1;
    R0 = R0 - R1;                       // R0 already has the counter from above
    W[P0] = R0.L;                       // make sure to update debounce_counter_select
    CC = R0 == 0;                       // check if debounce counter is zero
    //if CC   JUMP SelectKeyDebounced;  // counter is zero, debounce it
    if !CC  JUMP SelectDebounceEnd;     // counter is not zero, just finish

SelectKeyDebounced:                     // the select key is debounced, meaning
                                        //   that it was pressed, so we set the
                                        //   select_key_flag to 1
    P0.L = select_key_flag;             // prepare to write to select_key_flag
    P0.H = select_key_flag;
    R0 = 1;                             // write 1 to select_key_flag to show
                                        //   that the select key has been pressed
    B[P0] = R0;                         // select_key_flag = 1
    
    P0.L = current_key;                 // prepare to copy current_key
    P0.H = current_key;
    R0 = B[P0];
    
    P0.L = key_code;                    // copy current_key to key_code
    P0.H = key_code;
    B[P0] = R0;

SelectDebounceEnd:
    R1 = [SP++];                        // restore registers
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;

._key_debounce_select.END:

/*
_key_debounce_shift

Description:        Debounces the shift key. NOTE: The push-button holds the 
                    value (ie locks), so I am checking for when it changes and
                    debouncing the change (ie making sure it actually changed)

Operation:          Check if the shift key state is changed by ANDing the PF 
                    values with the KEEP_PF14_MASK and comparing it the previous
                    key value. If the key is the same, then decrement
                    debounce_counter_shift if it is not zero. If the key is
                    different, set debounce_counter_shift to DEBOUNCE_TIME. If
                    debounce_counter_shift is zero, then change shift_key_value
                    to the opposite value.

Arguments:          None.
Return Value:       None.

Local Variables:    shift_key_current - R0,R2 - current value of PF14
Shared Variables:   debounce_counter_shift - debounce counter for shift key [W]
                    shift_key_value - SHIFT_KEY_PRESSED if shift key is pressed,
                                      SHIFT_KEY_NOT_PRESSED otherwise [B]
                    shift_key_prev  - contains the previous shift key value [W]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_key_debounce_shift:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    [--SP] = R2;

    // Get the PF values in R0.L
    [--SP] = RETS;					    // preserve return address
    CALL _get_PF;
    RETS = [SP++];                      // restore return address
    
    R1 = KEEP_PF14_MASK
    R0 = R0 & R1;                       // keep only PF14
    
    P0.L = shift_key_prev;              // we want to compare the current PF value
    P0.H = shift_key_prev;              //   to the one we had before
    R1.L = W[P0];                       // R1 contains shift_key_prev
    R1.H = 0;                           // clear any junk from R1.H
    
    CC = R1 == SHIFT_KEY_INIT;          // if shift_key_prev is the init value,
                                        //   just debounce the key because it
                                        //   is the first time we're doing this
    if CC   JUMP ShiftKeyPressed;        
    
    R0.H = 0;                           // clear high words of R0 and R1 to make 
    R1.H = 0;                           //   sure comparison is correct
    R2 = R0;                            // preserve the current value so we can
                                        //   write it to shift_key_prev later
    
    CC = R0 == R1;                      // check if the key is the same
    if CC   JUMP ShiftKeyPressed;       // key is the same, debounce
    //if !CC  JUMP ShiftKeyNotPressed;  // key is not the same, set counter to DEBOUNCE_TIME
    
ShiftKeyNotPressed:                     // key isn't the same, counter = DEBOUNCE_TIME
    P0.L = debounce_counter_shift;      // write to the shift debounce counter
    P0.H = debounce_counter_shift;
    R0.L = DEBOUNCE_TIME;               // prepare to write DEBOUNCE_TIME
    W[P0] = R0.L;                       // debounce_counter_shift = DEBOUNCE_TIME
    
    JUMP ShiftDebounceEnd;
    
ShiftKeyPressed:                        // key is the same, decrement counter if
                                        //   it is non-zero
    P0.L = debounce_counter_shift;      // read the shift debounce counter
    P0.H = debounce_counter_shift;
    R0.L = W[P0];                       // assign the counter temporarily to R0.L
    CC = R0 == 0;                       // check if debounce counter is zero
    if CC   JUMP ShiftDebounceEnd;      // it is, we've already debounced, just
                                        //   jump to the end because we're done
    //if !CC  JUMP DecrementShiftCounter;// it isn't, decrement it and check if
                                        //   it's zero
    
DecrementShiftCounter:                  // decrement the shift debounce counter
    R1 = 1;
    R0 = R0 - R1;                       // R0 already has the counter from above
    W[P0] = R0.L;                       // make sure to update debounce_counter_select
    CC = R0 == 0;                       // check if debounce counter is zero
    //if CC   JUMP ShiftKeyDebounced;   // counter is zero, debounce it
    if !CC  JUMP ShiftDebounceEnd;      // counter is not zero, just finish

ShiftKeyDebounced:                      // the shift key is debounced, meaning
                                        //   that it switched values, so we change
                                        //   the shift_key_value
    P0.L = shift_key_value;             // prepare to switch shift_key_value
    P0.H = shift_key_value;
    R0 = B[P0];                         // get the shift_key_value in R0
    R0 = - R0;                          // prepare for next step
    R1 = 1;
    R0 = R0 + R1;                       // HACK: doing 1 - the previous shift_key_value
                                        //   gives the opposite shift_key_value
                                        //   instead of doing a lot of if statements
    B[P0] = R0;                         // shift_key_flag = 1 - shift_key_value

ShiftDebounceEnd:
    P0.L = shift_key_prev;              // write the current shift key to shift_key_prev
    P0.H = shift_key_prev;
    W[P0] = R2.L;                       // shift_key_prev = shift_key_current
    
    R2 = [SP++];                        // restore registers
    R1 = [SP++];
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;

._key_debounce_shift.END:

/*
_key_current_value_change

Description:        Changes the value of current_key based on the direction the
                    rotary encoder was turned. If the direction was clockwise,
                    go to the next state (ie current_key++). Otherwise, go to
                    the previous state (ie current_key--).

Operation:          Read the PF register to see which way the encoder was turned.
                    If it was turned clockwise, increment current_key. If it
                    was turned counterclockwise, decrement current_key. Wrap to
                    the correct key if it goes past the first or last key in the
                    list. Write current_key, and add a time delay before the
                    rotary encoder can send another interrupt.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   current_key        - value of the key currently selected [B]
                    encoder_time_delay - time delay for the rotary encoder before
                                         it can cause interrupts after it has
                                         been rotated. [W]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_key_current_value_change:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    [--SP] = R2;
    
                                        // check if we are allowed to rotate by
                                        //   checking if encoder_time_delay is
                                        //   greater than zero
    P0.L = encoder_time_delay;          // get the address first
    P0.H = encoder_time_delay;
    R0.L = W[P0];                       // get encoder_time_delay in R0 
    R0.H = 0;                           // clear the high word just in case
    R1 = 0;                             // since we can't use greater than for
                                        //   some reason, we have to use R1 to
                                        //   compare with R0
    CC = R1 < R0;                       // check if we have encoder_time_delay > 0
    if CC   JUMP CurrentValueChangeEnd; // we do have a time delay, don't change
                                        //   the current key value
    //if !CC   JUMP ChangeCurrentValue;  // we don't have a time delay, change the
                                        //   current key value
                                     
ChangeCurrentValue:
                                            // now prevent the encoder from sending
                                        //   more interrupts for ENCODER_TIME_DELAY
                                        //   ms to prevent noise
    P0.L = encoder_time_delay;          // get the address first
    P0.H = encoder_time_delay;
    R0.L = ENCODER_TIME_DELAY;
    W[P0] = R0.L                        // encoder_time_delay = ENCODER_TIME_DELAY
    
    // first, get the PF values in R0.L
    [--SP] = RETS;					    // preserve return address
    CALL _get_PF;
    RETS = [SP++];                      // restore return address
    
    // NOTE: this is just a hacky way to keep both PF11 and PF12 without creating
    //       more random mask constants
    R2 = KEEP_PF11_MASK;
    R1 = R0 & R2;                       // keep PF 11
    R2 = KEEP_PF12_MASK;
    R0 = R0 & R2;                       // and PF 12
    R0 = R0 | R1;                       // OR them together to have them both
    
    R1 = ENCODER_PF_CW;                 // prepare to check if we went clockwise
    CC = R0 == R1;                      // check if we have rotated clockwise
    //if CC   JUMP Clockwise;           // we rotated clockwise
    if !CC  JUMP Counterclockwise;      // we rotated counterclockwise

Clockwise:                              // we rotated clockwise go to the next
                                        //   value for current_key
    P0.L = current_key;                 // prepare to write current_key
    P0.H = current_key;
    R0 = B[P0];                         // R0 contains current_key
    
    R1 = 1;
    R0 = R0 + R1;                       // HACK: incrementing current_key causes
                                        //   us to go to the next key (a table
                                        //   would otherwise be the best way to
                                        //   implement this)
    R1 = LAST_KEY;
    CC = R1 < R0;                       // check if we have gone past the last key
    //if CC   JUMP ClockwiseWrap;       // current_key is at NUM_KEYS, we have
                                        //   gone past the last key, so we wrap
                                        //   to the first key
    if !CC  JUMP UpdateCurrentKey       // we're within the bounds, just finish
    
ClockwiseWrap:                          // wrap to the first key
    R0 = FIRST_KEY;
    JUMP UpdateCurrentKey;

Counterclockwise:                       // we rotated counterclockwise, go to the
                                        //   previous value for current_key
    P0.L = current_key;                 // prepare to write current_key
    P0.H = current_key;
    R0 = B[P0];                         // R0 contains current_key
    
    R1 = 1;
    R0 = R0 - R1;                       // HACK: decrementing current_key causes
                                        //   us to go to the previous key (again,
                                        //   a table would otherwise be the best 
                                        //   way to implement this)
    CC = R0 < FIRST_KEY;                // check if we have gone past the first key
    //if CC   JUMP CounterclockwiseWrap;// current_key is at NUM_KEYS, we have
                                        //   gone past the last key, so we wrap
                                        //   to the first key
    if !CC  JUMP UpdateCurrentKey       // we're within the bounds, just finish
    
CounterclockwiseWrap:                   // wrap to the last key
    R0 = LAST_KEY;
    //JUMP UpdateCurrentKey;

UpdateCurrentKey:
    B[P0] = R0;                         // make sure to write current_key
    
    // update the display with new encoder status
    [--SP] = RETS;                      // preserve return address
    CALL _display_encoder;
    RETS = [SP++];                      // restore return address
    
                                        // now prevent the encoder from sending
                                        //   more interrupts for ENCODER_TIME_DELAY
                                        //   ms to prevent noise
    P0.L = encoder_time_delay;          // get the address first
    P0.H = encoder_time_delay;
    R0.L = ENCODER_TIME_DELAY;
    W[P0] = R0.L                        // encoder_time_delay = ENCODER_TIME_DELAY

    
CurrentValueChangeEnd:    
    R2 = [SP++];                        // restore registers
    R1 = [SP++];
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;

._key_current_value_change.END:

/*
_encoder_time_debounce

Description:        Debounces the encoder time delay.

Operation:          If the encoder_time_delay is non-zero, decrement it.

Arguments:          None.
Return Value:       None.

Local Variables:    None.
Shared Variables:   encoder_time_delay - time delay for the rotary encoder before
                                         it can cause interrupts after it has
                                         been rotated. [W]
Global Variables:   None.

Input:              None.
Output:             None.

Error Handling:     None.

Algorithms:         None.
Data Structures:    None.

Registers Changed:  None.

Revision History:
    4/29/16   Dennis Shim      initial revision
*/
_encoder_time_debounce:
    [--SP] = P0;                        // preserve registers
    [--SP] = R0;
    [--SP] = R1;
    [--SP] = ASTAT;

    P0.L = encoder_time_delay;          // get the address first
    P0.H = encoder_time_delay;
    R0.L = W[P0];                       // get encoder_time_delay in R0 
    R0.H = 0;                           // clear the high word just in case
    
    CC = R0 == 0;                       // check if we have encoder_time_delay = 0
    if CC   JUMP EncoderTimeDebounceEnd;// encoder_time_delay = 0, don't decrement
    //if !CC  JUMP DecrementEncoderTime;// encoder_time_delay > 0, decrement
    
DecrementEncoderTime:
    R1 = 1;                             // prepare to decrement encoder_time_delay
    R0 = R0 - R1;
    
    W[P0] = R0;                         // make sure to write encoder_time_delay
                                        //   after we changed it

EncoderTimeDebounceEnd:
    ASTAT = [SP++];                     // restore registers
    R1 = [SP++];
    R0 = [SP++];
    P0 = [SP++];
    
    RTS;

._encoder_time_debounce.END:

// Data segment
.section .data
debounce_counter_shift:             // 16-bit, stores counter for debouncing
                                    //   the shift keypress
    .space 2

debounce_counter_select:            // 16-bit, stores counter for debouncing
                                    //   the select keypress
    .space 2
select_key_flag:                    // 8-bit, 0 if there is select key available,
                                    //   1 otherwise
    .space 1
shift_key_value:                    // 8-bit, SHIFT_KEY_PRESSED if there is shift
                                    //   key available, SHIFT_KEY_NOT_PRESSED otherwise
    .space 1
shift_key_prev:                     // 16-bit, contains the previous value of the
                                    //   shift key
    .space 2
current_key:                        // 8-bit, value of the key that is currently
                                    //   being selected using the rotary encoder
    .space 1
key_code:                           // 8-bit, value of the key to return after
                                    //   _key_available is called
    .space 1
encoder_time_delay:                 // 16-bit, time delay for the rotary encoder
                                    //   after it has been rotated
    .space 2
.end
